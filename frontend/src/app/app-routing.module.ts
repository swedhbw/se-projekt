import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './core/services/auth.guard';

const routes: Routes = [
    {
        path: '',
        pathMatch: 'full',
        redirectTo: 'search',
        data: {
            breadcrumb: {
                label: 'Home',
            }
        }
    },
    {
        path: 'search',
        loadChildren: () => import('./pages/search/search.module').then(m => m.SearchModule),
        data: {
            breadcrumb: {
                label: 'Search',
            }
        }
    },
    {
        path: '404',
        loadChildren: () => import('./pages/not-found/not-found.module').then(m => m.NotFoundModule),
        data: {
            breadcrumb: {
                label: 'Page not found',
            }
        }
    },
    {
        path: 'signup',
        loadChildren: () => import('./pages/signup/signup.module').then(m => m.SignupModule),
        data: {
            breadcrumb: {
                label: 'Sign-Up',
            }
        }
    },
    {
        path: 'account',
        loadChildren: () => import('./pages/account/account.module').then(m => m.AccountModule),
        data: {
            breadcrumb: {
                label: 'Account',
            }
        },
        canActivate: [AuthGuard]
    },
    {
        path: 'info',
        loadChildren: () => import('./pages/info/info.module').then(m => m.InfoModule),
        data: {
            breadcrumb: {
                label: 'About-Us',
            }
        }
    },
    {
        path: 'recipes/:id',
        loadChildren: () => import('./pages/recipe/recipe.module').then(m => m.RecipeModule),
        data: {
            breadcrumb: {
                label: 'Recipe',
            }
        }
    },
    {
        path: 'login',
        loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule),
        data: {
            breadcrumb: {
                label: 'Login',
            }
        }
    },
    {
        path: 'cook-book',
        loadChildren: () => import('./pages/cook-book/cook-book.module').then(m => m.CookBookModule),
        data: {
            breadcrumb: {
                label: 'Cook Book',
            }
        },
        canActivate: [AuthGuard],
        
    },
    {
        path: '**',
        redirectTo: '404'
    },
];

@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})
export class AppRoutingModule { }

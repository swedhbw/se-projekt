import {Component, OnInit} from '@angular/core';
import {AuthService} from './core/services/auth.service';
import {UtilService} from './core/services/util.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit{
    title = 'frontend';

    constructor(private authService: AuthService, public utilService: UtilService) {
    }

    ngOnInit(): void {
        this.authService.setExpiration();
    }
}

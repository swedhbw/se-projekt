import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FormInputType} from '../../shared/models/form-input-type';
import {UtilService} from '../../core/services/util.service';
import {Recipe} from '../../shared/models/recipe.model';
import { ApiService } from 'src/app/core/services/api.service';
import { MappingService } from 'src/app/core/services/mapping.service';
import { ApiError } from 'src/app/shared/models/api.model';
import {IngredientWithoutId} from 'src/app/shared/models/ingredient.model';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.scss']
})
export class SearchComponent implements OnInit {

    ingredientForms: FormGroup[] = [];
    portionsForm!: FormGroup;
    ingredientArray: IngredientWithoutId[] = [];
    formInputType = FormInputType;
    units = this.mappingService.getUnitAsSelectModel();
    loading: boolean = false;

    recipes: Recipe[] = [];
    recipesTwo: Recipe[] = [];
    recipesOne: Recipe[] = [];
    ingredientProposals!: Observable<String[]>;

    constructor(
        private formBuilder: FormBuilder,
        public utilService: UtilService,
        private apiService: ApiService,
        private mappingService: MappingService) { }

    ngOnInit(): void {
        this.ingredientProposals = of(['No Data']);
        this.pushNewIngredient();
        this.onChange('');

        this.portionsForm = this.formBuilder.group({
            portions: [null, [Validators.required, Validators.min(1)]],
        });

        // Initial form data
        this.portions.setValue(1);
    }

    /**
     * Push a new ingredient form into the form list
     */
    pushNewIngredient(): void {
        if(this.ingredientForms.length < 15 && this.formsValid()) {
            this.ingredientForms.push(this.formBuilder.group({
                ingredient: [null, [Validators.required]],
                amount: [null, [Validators.required]],
                unit: [null, [Validators.required]]
            }));

            this.onChange('');
        }
    }

    // Getters for the controls
    _getIngredient(form: FormGroup): FormControl{
        return form.get('ingredient') as FormControl;
    }

    _getAmount(form: FormGroup): FormControl{
        return form.get('amount') as FormControl;
    }

    _getUnit(form: FormGroup): FormControl{
        return form.get('unit') as FormControl;
    }

    get portions(): FormControl{
        return this.portionsForm?.get('portions') as FormControl;
    }

    /**
     * Deletes the form for the single recipe row
     * @param index
     */
    popIngredient(index: number): void {
        if(this.ingredientForms.length > 1)
            this.ingredientForms.splice(index, 1);
    }

    /**
     * Checks if all sub forms are valid
     */
    formsValid(): boolean{

        for (const ingredientForm of this.ingredientForms) {
            if(!ingredientForm.valid){
                return false;
            }
        }

        return true;
    }

    /**
     * Handles the search
     */
    onSearch() {

        if(this.formsValid()){
            this._startLoading();
            this.ingredientArray = [];
            const searchedPortions = this.portions.value;

            for (const ingredientForm of this.ingredientForms) {
                this.ingredientArray.push(
                {
                    name: this._getIngredient(ingredientForm).value,
                    amount: this._getAmount(ingredientForm).value,
                    unit: this._getUnit(ingredientForm).value,
                });


            }

            // Load recipes
            this.apiService.getRecipe(this.ingredientArray, this.portions.value)
            .subscribe({
                next: (res: any) => {

                    this.recipes = [];
                    this.recipesOne = [];
                    this.recipesTwo = [];
                    if(res.data['0'].length <= 0 && res.data['1'].length <= 0 && res.data['2'].length <= 0) {
                        this.utilService.showSnackbar(
                            'No results found!',
                            'warning',
                            'Search finished',
                            'close-outline',
                        );
                    } else {
                        if(res.data['0'].length > 0){
                            this._loadIngredients(res.data['0'], searchedPortions, 0);
                        }

                        if(res.data['1'].length > 0){
                            this._loadIngredients(res.data['1'], searchedPortions, 1);
                        }

                        if(res.data['2'].length > 0){
                            this._loadIngredients(res.data['2'], searchedPortions, 2);
                        }

                        this.utilService.showSnackbar(
                            'Here are your results!',
                            'success',
                            'Search finished',
                            'checkmark-outline',
                        );
                    }

                    this._endLoading();
                },
                error: (error: ApiError) => {

                    this.utilService.showSnackbar(
                        error.message,
                        'danger',
                        error.title,
                        'close-outline',
                    );
                    this._endLoading();
                }
            });
        }
        else{
            this.utilService.showSnackbar(
                'Please fill the form correctly!',
                'danger',
                'No valid input',
                'close-outline',
            );
        }
    }

    /**
     * Callback function for changing the input
     * @param value
     */
    onChange(value: String): void {
        const regExp = new RegExp(`.*${value.toLowerCase()}.*`, 'g');
        const regExpSort = new RegExp(`^${value.toLowerCase()}.*`, 'g');

        this.apiService.getAllIngredients().subscribe({
            next: result => {
                let data = result.data as String[];

                this.ingredientProposals = of(data.filter((text: String) => {
                    return regExp.test(text.toLowerCase() as string);
                }).sort((a: String, b :String) => {
                        const resultA = regExpSort.test(a.toLowerCase() as string);
                        const resultB = regExpSort.test(b.toLowerCase() as string);

                        if (resultA && resultB  || !resultA && !resultB) return 0;
                        else if (resultA && !resultB) return -1;
                        else return 1;
                    })
                );
            },
        });
    }

    /**
     * Loads all ingredients from an api result
     * @param recipeResult
     * @param searchedPortions
     * @param numOfArray
     * @private
     */
    private _loadIngredients(recipeResult: any, searchedPortions: number, numOfArray: number){
        for (const recipe of recipeResult) {
            // Fill the recipe array with the ingredients
            this.mappingService.getRecipeById(recipe.id, (res: Recipe) => {
                // Convert the ingredients to the current selected amount of portions.
                res.ingredients = this.mappingService.convertToPortions(res.ingredients, res.portions, searchedPortions);

                switch (numOfArray){
                    case 0:
                        this.recipes.push(res);
                        break;
                    case 1:
                        this.recipesOne.push(res);
                        break;
                    case 2:
                        this.recipesTwo.push(res);
                        break;
                }

            });
        }
    }

    /**
     * Starts the loading animations
     * @private
     */
    private _startLoading(): void {
        this.loading = true;
        this.ingredientForms.forEach(form => {
            form.disable();
        });
    }

    /**
     * End of the loading animation
     * @private
     */
    private _endLoading(): void {
        this.loading = false;
        this.ingredientForms.forEach(form => {
            form.enable();
        });
    }
}

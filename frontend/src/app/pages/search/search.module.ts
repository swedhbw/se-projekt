import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SearchRoutingModule } from './search-routing.module';
import { SearchComponent } from './search.component';
import {
    NbAutocompleteModule,
    NbButtonModule,
    NbIconModule,
    NbInputModule,
    NbSearchModule,
    NbSelectModule,
    NbSpinnerModule, NbTabsetModule
} from '@nebular/theme';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [
    SearchComponent
  ],
    imports: [
        CommonModule,
        SearchRoutingModule,
        NbSearchModule,
        ReactiveFormsModule,
        NbSelectModule,
        NbInputModule,
        FormsModule,
        NbButtonModule,
        NbIconModule,
        SharedModule,
        NbSpinnerModule,
        NbAutocompleteModule,
        NbTabsetModule,
    ]
})
export class SearchModule { }

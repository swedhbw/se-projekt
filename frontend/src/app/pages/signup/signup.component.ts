import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FormInputType} from '../../shared/models/form-input-type';
import {FormService} from '../../core/services/form.service';
import {ValidatorService} from '../../core/services/validator.service';
import {ApiError} from '../../shared/models/api.model';
import {ApiService} from '../../core/services/api.service';
import {AccountSignup} from '../../shared/models/account.model';
import {UtilService} from '../../core/services/util.service';
import {Router} from '@angular/router';

@Component({
    selector: 'app-signup',
    templateUrl: './signup.component.html',
    styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit, AfterViewInit {
    signupForm!: FormGroup;
    formInputType = FormInputType;
    loading: boolean = false;
    innerWidth!: number;
    innerHeight!: number;

    @ViewChild('view')
    view!: ElementRef;

    constructor(
        private formBuilder: FormBuilder,
        private apiService: ApiService,
        public utilService: UtilService,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.signupForm = this.formBuilder.group({
            username: [null, [Validators.required]],
            email: [null, [Validators.required, Validators.email]],
            password: [null, [Validators.required, ValidatorService.passwordComplexity]],
            passwordRepeat: [null, [Validators.required, this.repeatValidator]],
        });
    }

    ngAfterViewInit(): void {
        this.onResize();
    }


    get username(){
        return this.signupForm.get('username') as FormControl;
    }

    get email(){
        return this.signupForm.get('email') as FormControl;
    }

    get password(){
        return this.signupForm.get('password') as FormControl;
    }

    get passwordRepeat(){
        return this.signupForm.get('passwordRepeat') as FormControl;
    }

    /**
     * Submitting the form
     */
    onSubmit(): void{

        // Validate Form
        FormService.validateForm(this.signupForm);

        // Login if valid
        if(this.signupForm.valid){
            this._startLoading();

            const signupData: AccountSignup = {
                name: this.username.value,
                email: this.email.value,
                password: this.password.value
            };

            this.apiService.signup(signupData).subscribe({
                next: (value: any) => {
                    this.utilService.showSnackbar(
                        'Hello ' + value.data.name + ' your account is successfully generated!',
                        'success',
                        'Welcome ' + value.data.name,
                        'checkmark-outline',
                    );
                    this.router.navigate(['/login']).then(() => this.loading = false);
                },
                error: (error: ApiError) => {
                    this._endLoading();
                    this.utilService.showSnackbar(
                        error.message,
                        'danger',
                        error.title,
                        'close-outline',
                    );
                }
            });
        } else {
            this._clearPassword();
            this.utilService.showSnackbar(
                'Please fill the form correctly!',
                'danger',
                'No valid input',
                'close-outline',
            );
        }
    }

    /**
     * Chose the error text fitting the password validation error
     */
    chosePasswordError= () : String => {
        if(this.password.getError('hasNoUpperCase')) return 'Please use at least one capital letter';
        if(this.password.getError('hasNoLowerCase')) return 'Please use at least one normal letter';
        if(this.password.getError('hasNoNumeric')) return 'Please use at least one number';
        if(this.password.getError('length')) return 'Please use at least six digits';

        return 'Please enter a valid password';
    };

    /**
     * Validates if the passwords equals each other
     * @param control
     */
    repeatValidator = (control: FormControl): { [s: string]: boolean } => {
        if (!control.value) {
            return { required: true };
        }
        else if (control.value !== this.signupForm.controls['password'].value) {
            return { repeat: true, error: true };
        }
        return {};
    };

    /**
     * Clears the password fields of the form
     * @private
     */
    private _clearPassword(): void {
        this.password.reset();
        this.passwordRepeat.reset();
    }

    /**
     * Starts the loading animations
     * @private
     */
    private _startLoading(): void {
        this.loading = true;
        this.signupForm.disable();
    }

    /**
     * End of the loading animation
     * @private
     */
    private _endLoading(): void {
        this.loading = false;
        this.signupForm.enable();
        this._clearPassword();
    }

    /**
     * called on window resize and handles all resize action
     */
    onResize(): void{
        this.innerWidth = this.view.nativeElement.offsetWidth;
        this.innerHeight= this.view.nativeElement.offsetHeight;
    }
}

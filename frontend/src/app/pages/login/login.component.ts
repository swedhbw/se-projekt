import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {ApiService} from '../../core/services/api.service';
import {Router} from '@angular/router';
import {FormInputType} from '../../shared/models/form-input-type';
import {FormService} from '../../core/services/form.service';
import {ApiError} from '../../shared/models/api.model';
import {AuthService} from '../../core/services/auth.service';
import {UtilService} from '../../core/services/util.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit, AfterViewInit {
    loginForm!: FormGroup;
    formInputType = FormInputType;
    loading: boolean = false;
    innerWidth!: number;
    innerHeight!: number;

    @ViewChild('view')
    view!: ElementRef;

    constructor(
        private formBuilder: FormBuilder,
        private apiService: ApiService,
        private authService: AuthService,
        public utilService: UtilService,
        private router: Router,
    ) { }

    ngOnInit(): void {
        this.loginForm = this.formBuilder.group({
            email: [null, [Validators.required, Validators.email]],
            password: [null, [Validators.required]],
        });
    }

    ngAfterViewInit(): void {
        this.onResize();
    }

    // Getters for the controls

    get email(){
        return this.loginForm.get('email') as FormControl;
    }

    get password(){
        return this.loginForm.get('password') as FormControl;
    }

    /**
     * Submitting the form
     */
    onSubmit(): void{
        // Validate Form
        FormService.validateForm(this.loginForm);

        // Login if valid
        if(this.loginForm.valid){
            this._startLoading();

            // Send login data to backend and handle events
            this.authService.login(this.email.value, this.password.value)
                .subscribe({
                    next: () => {
                        this._clearForm();
                        this.utilService.showSnackbar(
                            'Successfully logged in!',
                            'success',
                            'Logged in',
                            'checkmark-outline',
                        );
                        this.authService.setExpiration();
                        this.router.navigate(['/cook-book']).then(() => this._endLoading());
                    },
                    error: (error: ApiError) => {
                        this._endLoading();
                        this.utilService.showSnackbar(
                            error.message,
                            'danger',
                            error.title,
                            'close-outline',
                        );
                    }
                });
        } else {
            this.utilService.showSnackbar(
                'Please fill the form correctly!',
                'danger',
                'No valid input',
                'close-outline',
            );
        }
    }

    /**
     * Clears the form
     * @private
     */
    private _clearForm(): void {
        this.loginForm.reset();
    }

    /**
     * Clears the password
     * @private
     */
    private _clearPassword(): void {
        this.password.reset();
    }

    /**
     * Starts the loading animations
     * @private
     */
    private _startLoading(): void {
        this.loading = true;
        this.loginForm.disable();
    }

    /**
     * End of the loading animation
     * @private
     */
    private _endLoading(): void {
        this.loading = false;
        this.loginForm.enable();
        this._clearPassword();
    }


    /**
     * called on window resize and handles all resize action
     */
     onResize(): void{
        this.innerWidth = this.view.nativeElement.offsetWidth;
        this.innerHeight= this.view.nativeElement.offsetHeight;
    }

}

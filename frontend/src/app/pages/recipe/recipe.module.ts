import {NgModule} from '@angular/core';
import { CommonModule } from '@angular/common';
import { RecipeRoutingModule } from './recipe-routing.module';
import { RecipeComponent } from './recipe.component';
import {
    NbButtonModule,
    NbIconModule,
    NbTooltipModule,
    NbSpinnerModule,
    NbInputModule,
    NbFormFieldModule
} from '@nebular/theme';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [
    RecipeComponent
  ],
    imports: [
        CommonModule,
        RecipeRoutingModule,
        NbButtonModule,
        NbTooltipModule,
        NbIconModule,
        NbSpinnerModule,
        FormsModule,
        SharedModule,
        NbInputModule,
        ReactiveFormsModule,
        NbFormFieldModule
    ]
})
export class RecipeModule {}

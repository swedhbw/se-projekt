import {Component, OnInit} from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {Recipe} from '../../shared/models/recipe.model';
import {Difficulty} from '../../shared/models/difficulty.model';
import { ApiError } from 'src/app/shared/models/api.model';
import { ApiService } from 'src/app/core/services/api.service';
import { MappingService } from 'src/app/core/services/mapping.service';
import { UtilService } from 'src/app/core/services/util.service';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FormInputType} from '../../shared/models/form-input-type';
import {FormService} from '../../core/services/form.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-recipe',
  templateUrl: './recipe.component.html',
  styleUrls: ['./recipe.component.scss']
})
export class RecipeComponent implements OnInit {

    recipe: Recipe | undefined;
    recipeId: number | undefined;
    isFavorite: boolean | undefined;
    portionsForm!: FormGroup;
    difficulties = Difficulty;
    favoriteLoading: { form: FormGroup | undefined, loading: boolean } = {form: undefined, loading: false};
    formInputType = FormInputType;
    apiUrl = environment.apiUrl;

    constructor(
        private route: ActivatedRoute,
        private apiService: ApiService,
        public utilService: UtilService,
        public mappingService: MappingService,
        private formBuilder: FormBuilder
    ) { }


    ngOnInit(): void {
        this.favoriteLoading.loading = false;

        this.portionsForm = this.formBuilder.group({
            portions: [null, [Validators.required, Validators.min(1)]]
        });

        this.route.params.subscribe(params => {
            this.recipeId = params['id'];
        });

        this.route.queryParams.subscribe({
           next: queryParams => {
               if(queryParams['portions']) this.portions.setValue(queryParams['portions']);
           }
        });

        this.loadRecipe();

    }

    // Form Control getter
    get portions(): FormControl{
        return this.portionsForm.get('portions') as FormControl;
    }

    /**
     * Reloads the recipe
     */
    loadRecipe() :void {
        if(this.recipeId || this.recipeId == 0){
            this.mappingService.getRecipeById(this.recipeId, (res: Recipe) => {

                // Only if the portions are set
                if(this.portions.value && this.portions.value > 0){
                    res.ingredients = this.mappingService.convertToPortions(res.ingredients, res.portions, this.portions.value);
                } else {
                    this.portions.setValue(res.portions);
                }
                this.recipe = res;
            });
        }
    }

    /**
     * Adds or removes the recipe from or to the users favorites
     */
     onFavorite(): void {
        if(!this.recipe!.isFavorite) {
            this._startLoading(this.favoriteLoading);
            this.apiService.createFavourite(this.recipe!.id).subscribe({
                next: () => {
                    this.recipe!.isFavorite = true;
                    this.utilService.showSnackbar(
                        'Successfully favorited recipe!',
                        'success',
                        'New favorited Recipe',
                        'checkmark-outline',
                    );
                    this._endLoading(this.favoriteLoading);
                },
                error: (error: ApiError) => {
                    this.utilService.showSnackbar(
                        error.message,
                        'danger',
                        error.title,
                        'close-outline',
                    );
                    this._endLoading(this.favoriteLoading);
                }
            });
        } else {
            this.apiService.deleteFavourite(this.recipe!.id).subscribe({
                next: () => {
                    this.recipe!.isFavorite = false;
                    this.utilService.showSnackbar(
                        'Successfully deleted favorite recipe!',
                        'success',
                        'Deleted favorited Recipe',
                        'checkmark-outline',
                    );
                    this._endLoading(this.favoriteLoading);
                },
                error: (error: ApiError) => {
                    this.utilService.showSnackbar(
                        error.message,
                        'danger',
                        error.title,
                        'close-outline',
                    );
                    this._endLoading(this.favoriteLoading);
                }
            });
        }

    }

    /**
     * Handles the onChange of the portions form
     */
    onChange(): void {
        FormService.validateForm(this.portionsForm);

        if(this.portionsForm.valid) {
            this.loadRecipe();
        }
    }

    /**
     * Starts the loading animations
     * @private
     */
    private _startLoading(formLoading: {form: FormGroup | undefined, loading: boolean}): void {
        formLoading.loading = true;
    }

    /**
     * End of the loading animations
     * @private
     */
    private _endLoading(formLoading: {form: FormGroup | undefined, loading: boolean}): void {
        formLoading.loading = false;
    }
}

import { Component, OnInit } from '@angular/core';
import {UtilService} from '../../core/services/util.service';

@Component({
  selector: 'app-not-found',
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent {

  constructor(public utilService: UtilService) { }

}

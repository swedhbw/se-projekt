import { Component, OnInit } from '@angular/core';
import {UtilService} from '../../core/services/util.service';

@Component({
  selector: 'app-info',
  templateUrl: './info.component.html',
  styleUrls: ['./info.component.scss']
})
export class InfoComponent {

  constructor(public utilService: UtilService) { }

}

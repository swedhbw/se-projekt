import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EditRoutingModule } from './edit-routing.module';
import { EditComponent } from './edit.component';
import {CreateModule} from '../create/create.module';


@NgModule({
  declarations: [
    EditComponent
  ],
    imports: [
        CommonModule,
        EditRoutingModule,
        CreateModule
    ]
})
export class EditModule { }

import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ApiService} from '../../../core/services/api.service';
import {Recipe, RecipeNew} from '../../../shared/models/recipe.model';
import {UtilService} from '../../../core/services/util.service';
import {MappingService} from '../../../core/services/mapping.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
    loading: boolean = true;
    recipe?: Recipe;
    recipeId?: number;

    constructor(
        private route: ActivatedRoute,
        private apiService: ApiService,
        private utilService: UtilService,
        private router: Router,
        private mappingService: MappingService,
    ) { }

    ngOnInit(): void {
        this.route.params.subscribe({
            next: params => {
                this.recipeId = params['recipeId'] as number;
                this._loadRecipe();
            }
        });

    }

    /**
     * Loads the recipe and sets after this loading on false
     * Should only be triggered after id is initialized
     * @private
     */
    private _loadRecipe() {
        try {
            this.mappingService.getRecipeById(this.recipeId!, (recipe: Recipe) => {
                this.recipe = recipe;
                this.loading = false;
            });
        } catch (e) {
            this.router.navigate(['/cook-book']).then();
        }

    }
}

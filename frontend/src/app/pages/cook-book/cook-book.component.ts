import { Component, OnInit } from '@angular/core';
import {Recipe} from '../../shared/models/recipe.model';
import { ApiService } from 'src/app/core/services/api.service';
import { MappingService } from 'src/app/core/services/mapping.service';
import { UtilService } from 'src/app/core/services/util.service';
import { ApiError } from 'src/app/shared/models/api.model';
import {FormBuilder, FormControl, FormGroup} from '@angular/forms';
import {FormInputType} from '../../shared/models/form-input-type';
import {Observable, of} from 'rxjs';
import {FormService} from '../../core/services/form.service';


@Component({
  selector: 'app-cook-book',
  templateUrl: './cook-book.component.html',
  styleUrls: ['./cook-book.component.scss']
})
export class CookBookComponent implements OnInit {
    ownRecipes: Recipe[] = [];
    favRecipes: Recipe[] = [];
    ownRecipesOriginal: Recipe[] = [];
    favRecipesOriginal: Recipe[] = [];
    searchForm!: FormGroup;
    formInputType = FormInputType;
    searchProposals!: Observable<Recipe[]>;
    favRecipesExist = false;

    loading: boolean = true;


    constructor(
        private apiService: ApiService,
        private mappingService: MappingService,
        public utilService: UtilService,
        private formBuilder: FormBuilder,
    ) {
    }

    ngOnInit(): void {
        this.searchForm = this.formBuilder.group({
            search: [null]
        });

        this.updateRecipes();

        this.apiService.getFavourite()
        .subscribe({
            next: (res: any) => {
                if (res.data.length > 0) {
                    this.favRecipesExist = true;
                }
            },
            error: (error: ApiError) => {
                this.utilService.showSnackbarError(error);
            }
        });

        this.searchProposals = of([]);
    }

    /**
     * Get the search form control
     */
    get search() {
        return this.searchForm.get('search') as FormControl;
    }

    updateRecipes() {
        this.getOwnRecipes();
        this.getFavRecipes();
    }

    getOwnRecipes() {
        this.apiService.getOwnRecipes()
            .subscribe({
                next: (res: any) => {

                    this.ownRecipes = [];
                    this.ownRecipesOriginal = [];
                    for (const recipe of res.data) {
                        this.mappingService.getRecipeById(recipe.id, (res: Recipe) => {
                            this.ownRecipes.push(res);
                            this.ownRecipesOriginal.push(res);
                        });

                    }

                    this.loading = false;

                },
                error: (error: ApiError) => {

                    this.utilService.showSnackbarError(error);
                    this.loading = false;
                }
            });
    }

    getFavRecipes() {
        this.apiService.getFavourite()
            .subscribe({
                next: (res: any) => {

                    this.favRecipes = [];
                    this.favRecipesOriginal = [];
                    for (const recipe of res.data) {
                        this.mappingService.getRecipeById(recipe.recipeID, (res: Recipe) => {

                            this.favRecipes.push(res);
                            this.favRecipesOriginal.push(res);
                        });
                        this.loading = false;
                    }
                },
                error: (error: ApiError) => {

                    this.utilService.showSnackbarError(error);
                    this.loading = false;
                }
            });
    }

    updateRecipe(id: number) {

        let foundOwn = this.ownRecipesOriginal.findIndex(recipe => recipe.id === id);
        let foundFav = this.favRecipesOriginal.findIndex(recipe => recipe.id === id);

        this.loading = true;

        this.apiService.getRecipeById(id).subscribe({
            next: (recipe: any) => {
                this.favRecipes = [];

                this.ownRecipesOriginal[foundOwn].isFavorite = recipe.data.isFavorite;

                if (recipe.data.isFavorite && foundFav === -1) {
                    this.favRecipesOriginal.push(this.ownRecipesOriginal[foundOwn]);
                } else {
                    this.favRecipesOriginal.splice(foundFav, 1);
                }
                this.loading = false;
                this.onChange('');
            },
            error: (error: any) => {
                if (error.code == 404) {

                    this.ownRecipesOriginal.splice(foundOwn, 1);
                    this.favRecipesOriginal.splice(foundFav, 1);
                } else {
                    this.utilService.showSnackbarError(error);
                }
                this.loading = false;
                this.onChange('');
            }
        });
    }

    /**
     * Handles the change of the search field
     * @param value
     */
    onChange(value: String) {
        const regexFilter = new RegExp(`.*${value.toLowerCase()}.*`);
        const regexSort = new RegExp(`^${value.toLowerCase()}.*`);

        this.ownRecipes = [];
        this.favRecipes = [];

        this.ownRecipes = this.ownRecipesOriginal.filter((recipe) => {
           return regexFilter.test(recipe.name.toLowerCase());
        });

        this.favRecipes = this.favRecipesOriginal.filter((recipe) => {
            return regexFilter.test(recipe.name.toLowerCase());
        });

        let proposal = this.ownRecipes.filter((recipe) => {
            for (const favRecipe of this.favRecipes) {
                if(favRecipe.name === recipe.name) return false;
            }

            return true;
        });

        this.searchProposals = of(this.favRecipes.concat(proposal).sort((a, b) => {
            const resultA = regexSort.test(a.name.toLowerCase());
            const resultB = regexSort.test(b.name.toLowerCase());

            if (resultA && resultB  || !resultA && !resultB) return 0;
            else if (resultA && !resultB) return -1;
            else return 1;
        }));
    }
}

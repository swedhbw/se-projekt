import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CookBookRoutingModule } from './cook-book-routing.module';
import { CookBookComponent } from './cook-book.component';
import {
    NbAccordionModule,
    NbAutocompleteModule,
    NbButtonModule,
    NbIconModule,
    NbSpinnerModule,
    NbTabsetModule, NbTooltipModule
} from '@nebular/theme';
import {SharedModule} from '../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
  declarations: [
    CookBookComponent
  ],
    imports: [
        CommonModule,
        CookBookRoutingModule,
        NbButtonModule,
        NbIconModule,
        NbAccordionModule,
        SharedModule,
        NbSpinnerModule,
        NbTabsetModule,
        FormsModule,
        NbAutocompleteModule,
        ReactiveFormsModule,
        NbTooltipModule
    ]
})
export class CookBookModule { }

import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CookBookComponent } from './cook-book.component';

const routes: Routes = [
    {
        path: '',
        component: CookBookComponent
    },
    {
        path: 'create',
        loadChildren: () => import('./create/create.module').then(m => m.CreateModule),
        data: {
            breadcrumb: {
                label: 'Create Recipe',
            }
        }
    },
    {
        path: ':recipeId/edit',
        loadChildren: () => import('./edit/edit.module').then(m => m.EditModule),
        data: {
            breadcrumb: {
                label: 'Edit Recipe',
            }
        }
    },


];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class CookBookRoutingModule { }

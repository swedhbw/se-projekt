import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CreateRoutingModule } from './create-routing.module';
import { CreateComponent } from './create.component';
import {
    NbAutocompleteModule,
    NbButtonModule,
    NbCardModule,
    NbCheckboxModule, NbIconModule,
    NbInputModule,
    NbLayoutModule,
    NbSelectModule,
    NbSidebarModule, NbSpinnerModule, NbTooltipModule
} from '@nebular/theme';
import {SharedModule} from '../../../shared/shared.module';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';


@NgModule({
    declarations: [
        CreateComponent
    ],
    exports: [
        CreateComponent
    ],
    imports: [
        CommonModule,
        CreateRoutingModule,
        NbLayoutModule,
        NbCardModule,
        NbSelectModule,
        NbSidebarModule.forRoot(),
        NbCheckboxModule,
        NbInputModule,
        NbButtonModule,
        SharedModule,
        ReactiveFormsModule,
        FormsModule,
        NbIconModule,
        NbTooltipModule,
        NbSpinnerModule,
        NbAutocompleteModule
    ]
})
export class CreateModule { }

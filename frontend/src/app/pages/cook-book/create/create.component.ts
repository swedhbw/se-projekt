import {Component, Input, OnInit} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {Ingredient} from '../../../shared/models/ingredient.model';
import {FormInputType} from '../../../shared/models/form-input-type';
import {SelectModel} from '../../../shared/models/select.model';
import {FormService} from '../../../core/services/form.service';
import {UtilService} from '../../../core/services/util.service';
import {Recipe, RecipeNew} from '../../../shared/models/recipe.model';
import {Difficulty} from '../../../shared/models/difficulty.model';
import {ApiService} from '../../../core/services/api.service';
import {Router} from '@angular/router';
import {MappingService} from '../../../core/services/mapping.service';
import {Observable, of} from 'rxjs';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
    // Input for edit
    @Input() isEdit?: boolean = false;
    @Input() recipeEdit?: Recipe;

    // Forms
    recipeForm!: FormGroup;
    ingredientForm!: FormGroup;
    units: SelectModel[] = this.mappingService.getUnitAsSelectModel();
    ingredientProposal!: Observable<String[]>;

    // Additional data
    pictureFile: File | undefined = undefined;
    ingredients: Ingredient[] = [];
    loading: boolean = false;
    title: string = 'Create new recipe';
    buttonDescription: string = 'Create Recipe';
    imageUrl: string = environment.apiUrl + '/recipes/-1/images';

    // Constant data
    formInputType = FormInputType;
    difficulties!: SelectModel[];

    constructor(
        private formBuilder: FormBuilder,
        public utilService: UtilService,
        private apiService: ApiService,
        private router: Router,
        public mappingService: MappingService,
    ) { }

    ngOnInit(): void {
        // Init data
        this.difficulties = [
            {display: 'Easy', value: Difficulty.easy},
            {display: 'Medium', value: Difficulty.medium},
            {display: 'Hard', value: Difficulty.hard},
        ];

        this.recipeForm = this.formBuilder.group({
            name: [null, [Validators.required]],
            difficulty: [null, [Validators.required]],
            totalTime: [null, [Validators.required]],// In minutes
            prepTime: [null, [Validators.required]],
            portion: [null, [Validators.required, Validators.min(1)]],
            isPublic: [null],
            picture: [null],
            description: [null, [Validators.required]],
        });

        this.ingredientForm = this.formBuilder.group({
           name: [null, [Validators.required]],
           amount: [null, [Validators.required, Validators.min(0)]],
           unit: [null, [Validators.required]],
        });

        this.ingredientProposal = of([]);
        this.onChange('');

        // Initialize
        this.isPublic.setValue(false);
        this.portion.setValue(1);


        // Edit case
        if(this.isEdit) {
            this.title = 'Edit Recipe';
            this.buttonDescription = 'Edit Recipe';
            this.ingredients = this.recipeEdit!.ingredients;
            this.recipeName.setValue(this.recipeEdit?.name);
            this.difficulty.setValue(this.recipeEdit?.difficulty);
            this.totalTime.setValue(this.recipeEdit?.totalTime);
            this.prepTime.setValue(this.recipeEdit?.prepTime);
            this.portion.setValue(this.recipeEdit?.portions);
            this.isPublic.setValue(this.recipeEdit?.isPublic);
            this.description.setValue(this.recipeEdit?.instructions);
            this.imageUrl = environment.apiUrl + '/recipes/' + this.recipeEdit?.id + '/images' ;
        }
    }

    // Getters for the controls

    // Recipe Form Group
    get recipeName(){
        return this.recipeForm.get('name') as FormControl;
    }
    get difficulty(){
        return this.recipeForm.get('difficulty') as FormControl;
    }
    get totalTime(){
        return this.recipeForm.get('totalTime') as FormControl;
    }
    get prepTime(){
        return this.recipeForm.get('prepTime') as FormControl;
    }
    get portion(){
        return this.recipeForm.get('portion') as FormControl;
    }
    get isPublic(){
        return this.recipeForm.get('isPublic') as FormControl;
    }
    get picture(){
        return this.recipeForm.get('picture') as FormControl;
    }
    get description(){
        return this.recipeForm.get('description') as FormControl;
    }

    // Ingredient Form Group
    get ingredientName(){
        return this.ingredientForm.get('name') as FormControl;
    }
    get amount(){
        return this.ingredientForm.get('amount') as FormControl;
    }
    get unit(){
        return this.ingredientForm.get('unit') as FormControl;
    }



    // Functions
    /**
     * Adds the ingredient to the list of ingredients
     */
    onAddIngredient(): void {
        // Validate Form
        FormService.validateForm(this.ingredientForm);

        // Add ingredient to array if valid
        if(this.ingredientForm.valid){

            const newIngredient: Ingredient = {
                amount: this.amount.value,
                name: this.ingredientName.value,
                unit: this.unit.value,
            };

            this.ingredients.push(newIngredient);
            this.onChange('');
            this.ingredientForm.reset();
            this.ingredientForm.markAsPristine();

            this.utilService.showSnackbar(
                'Ingredient added successfully',
                'success',
                'Added',
                'checkmark-outline'
            );

        } else {
            this.utilService.showSnackbar(
                'Please fill the form correctly!',
                'danger',
                'No valid input',
                'close-outline',
            );
        }
    }

    /**
     * Delete the ingredient on the index in the ingredients array
     * @param index
     */
    onDeleteIngredient(index: number){
        if(index < 0){
            this.utilService.showSnackbar(
                'Unknown Error',
                'danger',
                'Please inform the developers',
                'close-outline',
            );

            throw Error('Index bellow zero');
        }
        this.ingredients.splice(index, 1);
    }

    /**
     * Handles to submit of the create form
     */
    onSubmit(): void{
        // Validate Form
        FormService.validateForm(this.recipeForm);

        // Add ingredient to array if valid
        if(this.recipeForm.valid){
            this._startLoading();

            if (this.isEdit) {
                this._edit();
            } else {
                this._create();
            }

        } else {
            this.utilService.showSnackbar(
                'Please fill the form correctly!',
                'danger',
                'No valid input',
                'close-outline',
            );
        }
    }

    onFileUpload(event: any): void {

        // this.formData.append(event.target.files.name, event.target.files[0]);
        this.pictureFile = event.target.files[0];

        let reader = new FileReader();

        this.pictureFile = event.target.files[0];
        if (this.pictureFile){
            reader.readAsDataURL(this.pictureFile);
        }
        reader.onload = () => {
            if(typeof reader.result == 'string'){
                this.imageUrl = reader.result;
            }

        };
    }



    /**
     * Handles the edit of a new recipe
     */
    private _create(): void {

        const newRecipe: RecipeNew = {
            difficulty: this.difficulty.value,
            instructions: this.description.value,
            isPublic: this.isPublic.value,
            name: this.recipeName.value,
            prepTime: this.prepTime.value,
            portions: this.portion.value,
            totalTime: this.totalTime.value
        };

        this.apiService.newRecipe(newRecipe).subscribe({
            next: recipeValue => {
                this._addIngredientsAndImage(recipeValue.data.id, recipeValue.data.name , 'add', this.pictureFile);

            },
            error: err => {
                this._endLoading();
                this.utilService.showSnackbar(
                    err.message,
                    'danger',
                    err.title,
                    'close-outline',
                );
            }
        });
    }

    /**
     * Handles the creation of an edit recipe
     */
    private _edit(): void {
        const editRecipe: RecipeNew = {
            difficulty: this.difficulty.value,
            instructions: this.description.value,
            isPublic: this.isPublic.value,
            name: this.recipeName.value,
            prepTime: this.prepTime.value,
            portions: this.portion.value,
            totalTime: this.totalTime.value
        };

        this.apiService.editRecipe(this.recipeEdit!.id!, editRecipe).subscribe({
            next: recipeValue => {

                this.apiService.deleteIngredient(recipeValue.data.id).subscribe({
                    next: _ => {
                        this._addIngredientsAndImage(recipeValue.data.id, recipeValue.data.name, 'edit', this.pictureFile);
                    },
                    error: err => {
                        this._endLoading();
                        this.utilService.showSnackbar(
                            err.message,
                            'danger',
                            err.title,
                            'close-outline',
                        );
                    }
                });
            },
            error: err => {
                this._endLoading();
                this.utilService.showSnackbar(
                    err.message,
                    'danger',
                    err.title,
                    'close-outline',
                );
            }
        });
    }

    /**
     * Trys to add all ingredients given the id
     * @private
     * @param recipeId
     * @param name
     * @param type
     * @param pictureFile
     */
    private _addIngredientsAndImage(recipeId: number, name: String, type: string, pictureFile?: File) {
        this.apiService.newIngredient(this.ingredients, recipeId).subscribe({
            next: _ => {

                if (this.pictureFile) {

                    const newImage: FormData = new FormData();
                    newImage.append('recipePicture', this.pictureFile);
                    let apiCall: Observable<any>;

                    if(type == 'edit'){
                        apiCall = this.apiService.editImage(recipeId, newImage);
                    }
                    else {
                        apiCall = this.apiService.addImage(recipeId, newImage);
                    }

                    apiCall.subscribe({
                        next: _ => {
                            this._endLoading();

                            this.router.navigate(['/cook-book']).then(() => {
                                window.location.reload();
                                this.utilService.showSnackbar(
                                    'Recipe added successfully',
                                    'success',
                                    'Added',
                                    'checkmark-outline'
                                );
                            });

                        },
                        error: err => {
                            this._endLoading();
                            this.utilService.showSnackbar(
                                err.message,
                                'danger',
                                err.title,
                                'close-outline',
                            );
                        }
                    });
                }
                else {
                    this._endLoading();

                    this.utilService.showSnackbar(
                        'Recipe added successfully',
                        'success',
                        'Added',
                        'checkmark-outline'
                    );
                    this.router.navigate(['/cook-book']);
                }
            },
            error: err => {
                this._endLoading();
                this.utilService.showSnackbar(
                    err.message,
                    'danger',
                    err.title,
                    'close-outline',
                );
            }
        });
    }
    /**
     * Callback function for changing the input
     * @param value
     */
    onChange(value: String): void {
        const regExp = new RegExp(`.*${value.toLowerCase()}.*`, 'g');

        this.apiService.getAllIngredients().subscribe({
            next: result => {
                let data = result.data as String[];

                this.ingredientProposal = of(data.filter((text: String) => {
                    return regExp.test(text.toLowerCase() as string);
                }));
            },
        });
    }


    /**
     * Starts the loading animations
     * @private
     */
    private _startLoading(): void {
        this.loading = true;
        this.recipeForm.disable();
    }

    /**
     * End of the loading animation
     * @private
     */
    private _endLoading(): void {
        this.loading = false;
        this.recipeForm.enable();
    }
}

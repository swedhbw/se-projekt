import { Component, OnInit } from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {FormInputType} from '../../shared/models/form-input-type';
import {FormService} from '../../core/services/form.service';
import {ValidatorService} from '../../core/services/validator.service';
import {ApiError} from '../../shared/models/api.model';
import {ApiService} from '../../core/services/api.service';
import {AccountSignup} from '../../shared/models/account.model';
import { UtilService } from 'src/app/core/services/util.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.scss']
})
export class AccountComponent implements OnInit {
  userLoading: { form: FormGroup | undefined, loading: boolean } = {form: undefined, loading: false};
  emailLoading: { form: FormGroup | undefined, loading: boolean } = {form: undefined, loading: false};
  passwordLoading: { form: FormGroup | undefined, loading: boolean } = {form: undefined, loading: false};

  formInputType = FormInputType;
  curUser!: AccountSignup;

  constructor(
      private formBuilder: FormBuilder,
      private apiService: ApiService,
      public utilService: UtilService) {}

  ngOnInit(): void {
    this.updateUser();
    this.userLoading.loading = false;
    this.emailLoading.loading = false;
    this.passwordLoading.loading = false;

  }

/**
 * Sends API-Call to upated account
 * @param type
 */
  onSubmit(type : string): void{

    switch (type) {

      //Updates Username
      case 'newUsername':
        FormService.validateForm(this.userLoading.form!);

        if (this.userLoading.form!.valid){
          this._startLoading(this.userLoading);

          //API-Call
          this.apiService.patchAccount({name: this.username.value}).subscribe({
            next: () => {
                this.utilService.showSnackbar(
                    'Successfully changed username!',
                    'success',
                    'Changed User',
                    'checkmark-outline',
                );
                this.updateUser();
                this._endLoading(this.userLoading);
            },
            error: (error) => {
                this._endLoading(this.userLoading);
                this.utilService.showSnackbar(
                    error.message,
                    'danger',
                    error.title,
                    'close-outline',
                );
            }
          });
        } else {
            this.utilService.showSnackbar(
                'Please fill the form correctly!',
                'danger',
                'No valid input',
                'close-outline',
            );
        }
        break;

      //Updates Email
      case 'newEmail':
        FormService.validateForm(this.emailLoading.form!);

        if (this.emailLoading.form!.valid){
          this._startLoading(this.emailLoading);

          //API-Call
          this.apiService.patchAccount({email: this.email.value}).subscribe({
            next: () => {
                this.utilService.showSnackbar(
                    'Successfully changed E-Mail!',
                    'success',
                    'Changed E-Mail',
                    'checkmark-outline',
                );
                this.updateUser();
                this._endLoading(this.emailLoading);
            },
            error: (error) => {
                this._endLoading(this.emailLoading);
                this.utilService.showSnackbar(
                    error.message,
                    'danger',
                    error.title,
                    'close-outline',
                );
            }
          });
        } else {
            this.utilService.showSnackbar(
                'Please fill the form correctly!',
                'danger',
                'No valid input',
                'close-outline',
            );
        }

        break;

      //Updates Password
      case 'newPassword':
        FormService.validateForm(this.passwordLoading.form!);

        if (this.passwordLoading.form!.valid){
          this._startLoading(this.passwordLoading);
          //API-Call
          this.apiService.changePassword({
              currPassword: this.currentPassword.value,
              newPassword: this.newPassword.value
            }).subscribe({
              next: () => {
                  this.utilService.showSnackbar(
                      'Successfully changed password!',
                      'success',
                      'Changed Password',
                      'checkmark-outline',
                  );
                  this.updateUser();
                  this._endLoading(this.passwordLoading);
                  this._clearPassword();
              },
              error: (error) => {
                  this._endLoading(this.passwordLoading);
                  this._clearPassword();
                  this.utilService.showSnackbar(
                      error.message,
                      'danger',
                      error.title,
                      'close-outline',
                  );
              }
          });
        } else {
            this.utilService.showSnackbar(
                'Please fill the form correctly!',
                'danger',
                'No valid input',
                'close-outline',
            );
        }
        break;

      default:
          this.utilService.showSnackbar(
              'Unknown Error!',
              'danger',
              'Please inform the developers!',
              'close-outline',
          );
        break;

    }
  }

/**
 * Gets information of user via API-Call and updates forms
 */
  updateUser(){
    this.apiService.getAccount().subscribe({
      next: (value: any) => {

          this.userLoading.form = this.formBuilder.group({
          username: [null, [Validators.required, ValidatorService.sameValue(value.data.name)]],
        });

          this.emailLoading.form = this.formBuilder.group({
          email: [null, [Validators.required, Validators.email, ValidatorService.sameValue(value.data.email)]],
        });

          this.passwordLoading.form = this.formBuilder.group({
          currentPassword: [null, [Validators.required]],
          newPassword: [null, [Validators.required, ValidatorService.passwordComplexity]],
          newPasswordRepeat: [null, [Validators.required, this.repeatValidator]],
        });

        this.curUser = value.data;
        this.username.setValue(this.curUser.name);
        this.email.setValue(this.curUser.email);
      },
      error: (error: ApiError) => this.utilService.showSnackbar(
          error.message,
          'danger',
          error.title,
          'close-outline',
      ),
    });
  }

  //Getters for the controls

  get username(){
    return this.userLoading.form!.get('username') as FormControl;
  }

  get email(){
    return this.emailLoading.form!.get('email') as FormControl;
  }

  get currentPassword(){
    return this.passwordLoading.form!.get('currentPassword') as FormControl;
  }

  get newPassword(){
    return this.passwordLoading.form!.get('newPassword') as FormControl;
  }

  get newPasswordRepeat(){
    return this.passwordLoading.form!.get('newPasswordRepeat') as FormControl;
  }

  /**
   * Chose the error text fitting the password validation error
   */
  chosePasswordError = () : String => {
    if(this.newPassword.getError('hasNoUpperCase')) return 'Please use at least one capital letter';
    if(this.newPassword.getError('hasNoLowerCase')) return 'Please use at least one normal letter';
    if(this.newPassword.getError('hasNoNumeric')) return 'Please use at least one number';
    if(this.newPassword.getError('length')) return 'Please use at least six digits';

    return 'Please enter a valid password';
  };

  /**
   * Validates if the passwords equals each other
   * @param control
   */
      repeatValidator = (control: FormControl): { [s: string]: boolean } => {
      if (!control.value) {
          return { required: true };
      }
      else if (control.value !== this.passwordLoading.form!.controls['newPassword'].value) {
          return { repeat: true, error: true };
      }
      return {};
  };

    /**
     * Clears the password fields of the form
     * @private
     */
    private _clearPassword(): void {
        this.passwordLoading.form!.reset();
    }

    /**
     * Starts the loading animations
     * @private
     */
    private _startLoading(formLoading: {form: FormGroup | undefined, loading: boolean}): void {
        formLoading.loading = true;
        formLoading.form!.disable();
    }

    /**
     * End of the loading animations
     * @private
     */
    private _endLoading(formLoading: {form: FormGroup | undefined, loading: boolean}): void {
        formLoading.loading = false;
        formLoading.form!.enable();
    }
}

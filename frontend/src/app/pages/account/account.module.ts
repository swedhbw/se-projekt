import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { AccountRoutingModule } from './account-routing.module';
import { AccountComponent } from './account.component';
import {NbButtonModule, NbInputModule, NbPopoverModule, NbAccordionModule, NbSpinnerModule} from '@nebular/theme';
import {SharedModule} from '../../shared/shared.module';


@NgModule({
  declarations: [
    AccountComponent
  ],
    imports: [
        CommonModule,
        AccountRoutingModule,
        FormsModule,
        ReactiveFormsModule,
        NbInputModule,
        NbButtonModule,
        NbPopoverModule,
        NbAccordionModule,
        SharedModule,
        NbSpinnerModule
    ]
})
export class AccountModule { }

import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
    NbThemeModule,
    NbLayoutModule,
    NbIconModule,
    NbButtonModule,
    NbTooltipModule,
    NbCardModule, NbPopoverModule, NbInputModule, NbToastrModule, NbSpinnerModule, NbToggleModule, NbDialogModule
} from '@nebular/theme';
import { NbEvaIconsModule } from '@nebular/eva-icons';
import { FooterComponent } from './core/footer/footer.component';
import { HeaderComponent } from './core/header/header.component';
import {BreadcrumbModule} from 'xng-breadcrumb';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {SharedModule} from './shared/shared.module';
import {HttpInterceptor} from './shared/pipes/http.interceptor';
import { AuthGuard } from './core/services/auth.guard';

@NgModule({
  declarations: [
      AppComponent,
      FooterComponent,
      HeaderComponent
  ],
    imports: [
        BrowserModule,
        AppRoutingModule,
        BrowserAnimationsModule,
        HttpClientModule,
        NbThemeModule.forRoot({name: 'corporate'}),
        NbLayoutModule,
        NbEvaIconsModule,
        NbInputModule,
        NbIconModule,
        NbButtonModule,
        NbTooltipModule,
        BreadcrumbModule,
        NbCardModule,
        FormsModule,
        NbInputModule,
        ReactiveFormsModule,
        NbPopoverModule,
        SharedModule,
        NbToastrModule.forRoot(),
        NbToggleModule,
        NbDialogModule.forRoot(),
        NbSpinnerModule
    ],
    providers: [
        {
            provide: HTTP_INTERCEPTORS,
            useClass: HttpInterceptor,
            multi: true
        },
        AuthGuard
    ],
    bootstrap: [AppComponent]
})
export class AppModule { }

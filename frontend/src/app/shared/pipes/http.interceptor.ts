import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent
} from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable()
export class HttpInterceptor implements HttpInterceptor {

    constructor() {}

    // Intercepting the Http Request and adding to the header the jwt token, if the token exists
    intercept(req: HttpRequest<unknown>, next: HttpHandler): Observable<HttpEvent<unknown>> {
        const idToken = localStorage.getItem('id_token');

        if (idToken) {
            const cloned = req.clone({
                 headers: req.headers.set('Authorization',
                   'Bearer ' + idToken)
             });

             return next.handle(cloned);
        }
        else {
           return next.handle(req);
        }
    }
}

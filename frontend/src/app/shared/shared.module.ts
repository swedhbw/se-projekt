import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import { CustomFormFieldComponent } from './components/custom-form-field/custom-form-field.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {
    NbButtonModule,
    NbIconModule,
    NbInputModule,
    NbTooltipModule,
    NbSelectModule,
    NbCardModule,
    NbSpinnerModule,
    NbAutocompleteModule, NbTabsetModule
} from '@nebular/theme';
import { RecipeShortViewComponent } from './components/recipe-short-view/recipe-short-view.component';
import { RecipeListComponent } from './components/recipe-list/recipe-list.component';
import {RouterModule} from '@angular/router';
import { RecipeDeleteDialogComponent } from './components/recipe-delete-dialog/recipe-delete-dialog.component';


@NgModule({
  declarations: [
      CustomFormFieldComponent,
      RecipeShortViewComponent,
      RecipeListComponent,
      RecipeDeleteDialogComponent,
  ],
    imports: [
        CommonModule,
        ReactiveFormsModule,
        FormsModule,
        NbInputModule,
        NbSelectModule,
        NbIconModule,
        RouterModule,
        NbButtonModule,
        NbCardModule,
        NbTooltipModule,
        NbSpinnerModule,
        NbAutocompleteModule,
        NbTabsetModule
    ],
    exports: [
        CommonModule,
        CustomFormFieldComponent,
        RecipeListComponent,
        RecipeShortViewComponent
    ]
})
export class SharedModule {
}

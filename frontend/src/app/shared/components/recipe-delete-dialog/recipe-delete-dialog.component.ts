import { Component, OnInit, Output } from '@angular/core';
import { FormGroup } from '@angular/forms';
import { NbDialogConfig, NbDialogRef, NbDialogService } from '@nebular/theme';
import { ApiService } from 'src/app/core/services/api.service';
import { UtilService } from 'src/app/core/services/util.service';
import { FormInputType } from '../../models/form-input-type';

@Component({
  selector: 'app-recipe-delete-dialog',
  templateUrl: './recipe-delete-dialog.component.html',
  styleUrls: ['./recipe-delete-dialog.component.scss']
})

export class RecipeDeleteDialogComponent {

  loading: boolean = false;
  recipeId: number | undefined;
  formInputType = FormInputType;
  
  constructor(private dialogRef: NbDialogRef<any>, private apiService: ApiService, private utilService: UtilService) { }

  onDelete(): void {

    if(this.recipeId){
      this._startLoading();

      this.apiService.deleteRecipe(this.recipeId).subscribe({
        next: () => {
            this.utilService.showSnackbar(
                'Successfully deleted recipe!',
                'success',
                'Deleted Recipe',
                'checkmark-outline',
            );
            this._endLoading();
            this.dialogRef.close(true);
        },
        error: (error) => {
            this._endLoading();
            this.utilService.showSnackbar(
                error.message,
                'danger',
                error.title,
                'close-outline',
            );
            this.dialogRef.close(false);
        }
      });
    }
  }

  onCancel(): void {
    this.dialogRef.close();
  }

  /**
   * Starts the loading animations
   * @private
   */
  private _startLoading(): void {
    this.loading = true;
  }

  /**
   * End of the loading animation
   * @private
   */
  private _endLoading(): void {
      this.loading = false;
  }
}

import {
    AfterViewChecked,
    AfterViewInit,
    Component,
    ElementRef,
    EventEmitter,
    Input,
    OnInit,
    Output,
    ViewChild
} from '@angular/core';
import {Recipe} from '../../models/recipe.model';
import {Difficulty} from '../../models/difficulty.model';
import {UtilService} from '../../../core/services/util.service';
import {NbDialogService} from '@nebular/theme';
import {RecipeDeleteDialogComponent} from '../recipe-delete-dialog/recipe-delete-dialog.component';
import {Router} from '@angular/router';
import { AuthService } from 'src/app/core/services/auth.service';
import { ApiError } from '../../models/api.model';
import { ApiService } from 'src/app/core/services/api.service';
import { FormGroup } from '@angular/forms';
import {MappingService} from '../../../core/services/mapping.service';
import { environment } from 'src/environments/environment';

@Component({
  selector: 'app-recipe-short-view',
  templateUrl: './recipe-short-view.component.html',
  styleUrls: ['./recipe-short-view.component.scss']
})
export class RecipeShortViewComponent implements OnInit, AfterViewInit, AfterViewChecked {
    @Input() recipe!: Recipe;
    @Input() manageAble: boolean = false;
    @Input() targetPortions?: number;
    @Output() updateElement = new EventEmitter<number>();
    difficulties = Difficulty;
    apiUrl = environment.apiUrl;

    favoriteLoading: { form: FormGroup | undefined, loading: boolean } = {form: undefined, loading: false};
    innerWidth!: number;
    innerHeight!: number;

    @ViewChild('view')
    view!: ElementRef;

    constructor(
        private util: UtilService,
        private dialogService: NbDialogService,
        private router: Router,
        public authService: AuthService,
        private apiService: ApiService,
        public mappingService: MappingService
    ) {}

    ngOnInit(): void {
        this.favoriteLoading.loading = false;
    }

    ngAfterViewInit(): void {
        this.onResize();
    }

    ngAfterViewChecked(): void{
        this.onResize();
    }

    /**
     * Handles the favorite operation of a recipe
     */
    onDelete(): void {
        const dialogRef = this.dialogService.open(RecipeDeleteDialogComponent, { context: { recipeId: this.recipe.id } })
        .onClose.subscribe(deleted => {
            if (deleted) {
                this.updateElement.emit(this.recipe.id);
            }});
    }

    onFavorite(): void {
        this._startLoading(this.favoriteLoading);
        if(!this.recipe.isFavorite) {
            this.apiService.createFavourite(this.recipe.id).subscribe({
                next: () => {
                    this.recipe.isFavorite = true;
                    this.util.showSnackbar(
                        'Successfully favorited recipe!',
                        'success',
                        'New favorited Recipe',
                        'checkmark-outline',
                    );
                    this._endLoading(this.favoriteLoading);
                    this.updateElement.emit(this.recipe.id);
                },
                error: (error: ApiError) => {

                    this.util.showSnackbarError(error);
                    this._endLoading(this.favoriteLoading);
                }
            });
        } else {
            this.apiService.deleteFavourite(this.recipe.id).subscribe({
                next: () => {
                    this.recipe.isFavorite = false;
                    this.util.showSnackbar(
                        'Successfully deleted favorite recipe!',
                        'success',
                        'Deleted favorited Recipe',
                        'checkmark-outline',
                    );
                    this._endLoading(this.favoriteLoading);
                    this.updateElement.emit(this.recipe.id);
                },
                error: (error: ApiError) => {

                    this.util.showSnackbarError(error);
                    this._endLoading(this.favoriteLoading);
                }
            });
        }

    }

    /**
     * Handles the edit operation of a recipe
     */
    onEdit(): void{
        this.router.navigate([`/cook-book/${this.recipe.id}/edit`]).then();
    }

    /**
     * Handles the redirect to the clicked recipe
     */
    onRedirect(): void {
        if(this.targetPortions) {
            this.router.navigate(
                [`/recipes/${this.recipe.id}`],
                {queryParams: {portions: this.targetPortions}
                }).then();
        } else {
            this.router.navigate([`/recipes/${this.recipe.id}`]).then();
        }
    }

    /**
     * Starts the loading animations
     * @private
     */
    private _startLoading(formLoading: {form: FormGroup | undefined, loading: boolean}): void {
        formLoading.loading = true;
    }

    /**
     * End of the loading animations
     * @private
     */
    private _endLoading(formLoading: {form: FormGroup | undefined, loading: boolean}): void {
        formLoading.loading = false;
    }

    /**
     * called on window resize and handles all resize action
     */
    onResize(): void{
        this.innerWidth = this.view.nativeElement.offsetWidth;
        this.innerHeight= this.view.nativeElement.offsetHeight;
    }


}

import {Component, EventEmitter, Input, Output} from '@angular/core';
import {FormInputType} from '../../models/form-input-type';
import {FormControl} from '@angular/forms';
import {SelectModel} from '../../models/select.model';
import {NbAutocompleteComponent} from '@nebular/theme';



@Component({
  selector: 'app-custom-form-field',
  templateUrl: './custom-form-field.component.html',
  styleUrls: ['./custom-form-field.component.scss']
})
export class CustomFormFieldComponent {
    // Enum reference
    formInputType = FormInputType;

    // Inputs
    @Input() type!: FormInputType;
    @Input() control!: FormControl;
    @Input() errorMessage?: String;
    @Input() placeholder?: String;
    @Input() min?: number | undefined;
    @Input() max?: number | undefined;
    @Input() data?: [{display: String, value: String}] | undefined | SelectModel[];
    @Input() required: boolean | string = false;
    @Input() initialValue?: any;
    @Input() autocomplete?: NbAutocompleteComponent<any>;
    @Output() valueChange = new EventEmitter<void>();

    /**
     * Emits if something changes in the text field
     */
    onChange() {
        this.valueChange.emit();
    }


    constructor() { }

}

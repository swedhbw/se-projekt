import {Component, EventEmitter, Input, OnInit, Output} from '@angular/core';
import {Recipe} from '../../models/recipe.model';

@Component({
  selector: 'app-recipe-list',
  templateUrl: './recipe-list.component.html',
  styleUrls: ['./recipe-list.component.scss']
})
export class RecipeListComponent {
    constructor() { }

    @Input() recipes!: Recipe[];
    @Input() manageAble: boolean = false;
    @Input() targetPortions?: number;

    @Output() updateRecipe = new EventEmitter<number>();

    updateList(id: number) {
        this.updateRecipe.emit(id);
    }
    
}

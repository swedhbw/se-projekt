import {Time} from '@angular/common';
import {Ingredient} from './ingredient.model';
import {Difficulty} from './difficulty.model';

/**
 * Data structure of a recipe object
 */
export interface Recipe {
    id: number;
    name: string;
    instructions: string;
    author: string;
    isPublic: boolean;
    createdAt: Date;
    totalTime: number; // In minutes
    prepTime: number; // In minutes
    portions: number;
    difficulty: Difficulty;
    isFavorite?: boolean;
    ingredients: Ingredient[];
}

/**
 * Data structure of a recipe object
 */
export interface RecipeNew {
    // name, instructions, isPublic, picture, totalTime, prepTime, difficulty
    name: string;
    instructions: string;
    isPublic: boolean;
    totalTime: number; // In minutes
    prepTime: number; // In minutes
    portions: number;
    difficulty: Difficulty;
}

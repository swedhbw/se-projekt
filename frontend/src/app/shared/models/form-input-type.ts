export enum FormInputType {
    text = 'text',
    password = 'password',
    number = 'number',
    select = 'select',
    textArea = 'textArea',
}

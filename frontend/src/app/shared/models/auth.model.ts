export interface AuthResult{
    message: string,
    expiresIn: number,
    token: string,
}

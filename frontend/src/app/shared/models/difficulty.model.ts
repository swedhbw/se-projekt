/**
 * Data structure of a difficulty enum
 */
export enum Difficulty {
    easy,
    medium ,
    hard,
}

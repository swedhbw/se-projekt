/**
 * Dummy model for converting the API output
 */
export interface ApiOutput {
    data: any
}

/**
 * Model describing the data structure of an API error
 */
export interface ApiError{
    code: number
    message: string
    title: string
}


import { Unit } from './unit.model';

/**
 * Data structure of a ingredient object
 */
export interface Ingredient {
    id?: number;
    name: string;
    amount: number;
    unit: Unit;
}

export interface IngredientWithoutId {
    name: string;
    amount: number;
    unit: Unit;
}

// same as unit model in backend
export enum Unit {
    MILLILITER = 0,
    GRAM = 1,
    PIECE = 2,
    TABLESPOON = 3,
    TEASPOON = 4,
    DASH = 5
}

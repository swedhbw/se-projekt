/**
 * Model for the login object
 */
export interface Login {
    email: String,
    password: String
}

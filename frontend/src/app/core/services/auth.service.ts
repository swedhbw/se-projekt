import { Injectable } from '@angular/core';
import {ApiService} from './api.service';
import * as moment from 'moment';
import {AuthResult} from '../../shared/models/auth.model';
import {map, Observable, shareReplay} from 'rxjs';
import {now} from 'moment';
import {UtilService} from './util.service';
import {Router} from '@angular/router';

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    constructor(
        private api: ApiService,
        private util: UtilService,
        private router: Router
    ) { }

    /**
     * Logs the user in and generates the jwt token
     * @param email
     * @param password
     */
    login(email:string, password:string ): Observable<any> {
        return this.api.login({email: email, password: password})
            .pipe(
                map(response => AuthService._setSession(response.data)),
                shareReplay()
            );
    }

    /**
     * Refresh the current jwt token
     */
    refresh(): void {
        this.api.refreshToken().subscribe({
            next: (response) => {
                AuthService._setSession(response.data);
                this.setExpiration();
            },
            error: () => {
                this.logout();
                this.util.showSnackbar(
                    'Session ended with Error. Please login again',
                    'danger',
                    'Session ended',
                    'close-outline',
                );
            }
        });
    }

    /**
     * Saves the jwt token
     * @param authResult
     * @private
     */
    private static _setSession(authResult: AuthResult): void {
        const expiresAt = moment().add(authResult.expiresIn,'second');

        // safe the token
        localStorage.setItem('id_token', authResult.token);
        localStorage.setItem('expires_at', JSON.stringify(expiresAt.valueOf()) );
    }

    /**
     * Logs the user out
     */
    logout(): void {
        localStorage.removeItem('id_token');
        localStorage.removeItem('expires_at');

        this.router.navigate(['/login']).then();
    }

    /**
     * Checks whether the user is logged in
     */
    isLoggedIn(): boolean {
        const expiresAt = this.getExpiration();

        if(expiresAt){
            return moment().isBefore(expiresAt);
        } else {
            return false;
        }
    }

    /**
     * Checks whether the user is logged out
     */
    isLoggedOut(): boolean {
        return !this.isLoggedIn();
    }

    /**
     * Get the expiration moment of the token
     */
    getExpiration():moment.Moment | undefined  {
        const expiration = localStorage.getItem('expires_at');

        if(!expiration) {
            return undefined;
        }

        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }

    /**
     * Sets a timer for refreshing the jwt token
     */
    setExpiration(): void{
        const expiresIn = this.getExpiration()?.utc().valueOf();
        const pufferTime = 4*3600000; // Time until the backend should refresh the token (4h = 1h before token expires)
        const actualTime = moment(now()).utc().valueOf();

        if(expiresIn){
            if(!this.isLoggedIn()){
                this.logout();

            } else if(this.isLoggedIn() && expiresIn - pufferTime > actualTime){

                setTimeout(() => {
                    this.refresh();
                }, expiresIn - pufferTime - actualTime);

            } else {
                this.refresh();
            }
        }
    }
}

import { Injectable } from '@angular/core';
import {AbstractControl, FormControl, FormGroup, ValidationErrors, ValidatorFn} from '@angular/forms';
import {ApiService} from '../../core/services/api.service';

@Injectable({
  providedIn: 'root'
})
export class ValidatorService {
    constructor(private apiService: ApiService) { }

    static passwordMinLength: number = 6;

    /**
     * Validator for checking the password complexity:
     * 1. At least one capital letter
     * 2. At least one normal letter
     * 3. At least one number
     * 4. At least 6 digits length
     *
     * If the required rule is not fulfilled following error states will be set:
     * 1. hasNoUpperCase
     * 2. hasNoLowerCase
     * 3. hasNoNumeric
     * 4. length
     *
     * In all error cases following error states will be also set:
     * - error
     * - complexity
     *
     * Has the control no value following error state will be set:
     * - required
     *
     * @param control
     */
    public static passwordComplexity = (control: FormControl): { [s: string]: boolean } => {
        if (!control.value) {
            return { required: true };
        }
        const value = control.value;

        const hasUpperCase = /[A-Z]+/.test(value);
        if(!hasUpperCase){
            return { error: true, complexity: true, hasNoUpperCase: true };
        }

        const hasLowerCase = /[a-z]+/.test(value);
        if(!hasLowerCase){
            return { error: true, complexity: true, hasNoLowerCase: true };
        }

        const hasNumeric = /[0-9]+/.test(value);
        if(!hasNumeric){
            return { error: true, complexity: true, hasNoNumeric: true };
        }

        const correctLength = value.length >= ValidatorService.passwordMinLength;
        if(!correctLength){
            return { error: true, complexity: true, length: true };
        }

        return {};
    };

    /**
     * Validator for checking if Value is different from <oldValue>
     */
    public static sameValue(oldValue: String){
        return (control: FormControl): { [s: string]: boolean } => {
            if (!control.value) {
                return { required: true };
            }
            if (control.value == oldValue){
                return { error: true};
            }
            return {};
        };
    }
}

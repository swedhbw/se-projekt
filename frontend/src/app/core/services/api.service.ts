import {Injectable} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpResponse} from '@angular/common/http';
import {map, Observable, throwError} from 'rxjs';
import {catchError} from 'rxjs/operators';
import {environment} from '../../../environments/environment';
import {ApiError, ApiOutput} from '../../shared/models/api.model';
import {Login} from '../../shared/models/login.model';
import {AccountSignup} from '../../shared/models/account.model';
import {IngredientWithoutId} from '../../shared/models/ingredient.model';
import {Ingredient} from '../../shared/models/ingredient.model';

@Injectable({
    providedIn: 'root'
})
export class ApiService {

    private readonly baseUrl = environment.apiUrl;

    /**
     * Handles an API error and maps it into the used error format.
     * If other functions want to use an observable below implementing this function,
     * it can subscribe to the observable and use the error case for the wished output e.g. Snackbar
     * @param err
     */
    handleError(err: HttpErrorResponse): Observable<never>{
        let newError: ApiError = {
            code: err.status,
            message: err.error.message,
            title: 'Unknown Error'
        };

        if (err.status === 0) {
            newError.message = 'The server cannot be reached. Please try again later.';
            newError.title = 'Server offline';
        } else  {
            console.error(`Backend returned error code: ${err.status}. Body was: `, err.error);
            switch (err.status) {
                case 400:
                    newError.title = 'Request Body false';
                    break;
                case 401:
                    newError.title = 'Unauthorized';
                    break;
                case 404:
                    newError.title = 'Not Found';
                    break;
                case 403:
                    newError.title = 'Forbidden';
                    break;
                case 500:
                    newError.title = 'Internal Server error';
                    break;
                default:
                    newError.title = 'Unknown Error';
                    break;
            }
        }

        return throwError(() => newError);
    }

    constructor(private http: HttpClient) {
    }

    /**
     * Getting all recipes existing
     */
    getRecipe(ingredients: IngredientWithoutId[], portions: number): Observable<ApiOutput> {
        let body = {
            ingredients: ingredients,
            fromAccountId : null, //needs to be implemented for filtered search
            portions: portions,
        };
        return this.http.patch<HttpResponse<any>>(
            `${this.baseUrl}/recipes`,
            body,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to log in the user in the backend
     */
    login(loginData: Login): Observable<ApiOutput> {
        return this.http.post<HttpResponse<any>>(
            `${this.baseUrl}/auth/login`,
            loginData,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to generate a account in the backend
     */
    signup(signupData: AccountSignup): Observable<ApiOutput> {
        return this.http.post<HttpResponse<any>>(
            `${this.baseUrl}/accounts`,
            signupData,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Get a new token
     */
    refreshToken(): Observable<ApiOutput> {
        return this.http.get<HttpResponse<any>>(
            `${this.baseUrl}/auth/refresh`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Get account of user with JWT
     */
    getAccount(): Observable<ApiOutput> {
        return this.http.get<HttpResponse<any>>(
            `${this.baseUrl}/accounts`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }


    /**
     * Update account (Username and E-Mail)
     */
    patchAccount(changes: any): Observable<ApiOutput> {
        return this.http.patch<HttpResponse<any>>(
            `${this.baseUrl}/accounts`,
            changes,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Update account password
     */
    changePassword(passwords: any): Observable<ApiOutput> {
        return this.http.put<HttpResponse<any>>(
            `${this.baseUrl}/accounts/password`,
            passwords,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }





// <----------------- Recipe related routes beginning here ----------------->


    /**
    * Trys to get a recipe
    */
    getRecipeById(recipeId: number): Observable<ApiOutput> {
        return this.http.get<HttpResponse<any>>(
            `${this.baseUrl}/recipes/${recipeId}`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to generate a new user
     */
    newRecipe(newRecipe: any): Observable<ApiOutput> {
        return this.http.post<HttpResponse<any>>(
            `${this.baseUrl}/accounts/recipes`,
            newRecipe,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to generate a new user
     */
    addImage(recipeId: Number, newRecipe: FormData): Observable<ApiOutput> {
        return this.http.post<HttpResponse<any>>(
            `${this.baseUrl}/accounts/recipes/${recipeId}/images`,
            newRecipe,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to generate a new user
     */
    editImage(recipeId: Number, newRecipe: FormData): Observable<ApiOutput> {
        return this.http.put<HttpResponse<any>>(
            `${this.baseUrl}/accounts/recipes/${recipeId}/images`,
            newRecipe,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to patch the recipe with the given id for editing it
     */
    editRecipe(recipeId: number, changes: any): Observable<ApiOutput> {
        return this.http.patch<HttpResponse<any>>(
            `${this.baseUrl}/accounts/recipes/${recipeId}`,
            changes,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to delete the recipe with the given id
     */
    deleteRecipe(recipeId: number): Observable<ApiOutput> {
        return this.http.delete<HttpResponse<any>>(
            `${this.baseUrl}/accounts/recipes/${recipeId}`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Gets all favourites of a user
     */
    getOwnRecipes(): Observable<ApiOutput> {
        return this.http.get<HttpResponse<any>>(
            `${this.baseUrl}/accounts/recipes`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};

                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Gets all favourites of a user
     */
    getFavourite(): Observable<ApiOutput> {
        return this.http.get<HttpResponse<any>>(
            `${this.baseUrl}/accounts/favorites`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};

                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Gets all user who favourited the recipe
     */
    getAllFavourited(): Observable<ApiOutput> {
        return this.http.get<HttpResponse<any>>(
            `${this.baseUrl}/accounts/favorites`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to add a recipe as favoruite with the given id
     */
    createFavourite(recipeId: number): Observable<ApiOutput> {
        return this.http.post<HttpResponse<any>>(
            `${this.baseUrl}/accounts/favorites`,
            {recipeId: recipeId},
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to delete a recipe as favoruite with the given id
     */
    deleteFavourite(recipeId: number): Observable<ApiOutput> {
        return this.http.delete<HttpResponse<any>>(
            `${this.baseUrl}/accounts/favorites/${recipeId}`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
    * Gets all user who favourited the recipe
    */
    getIngredientsById(recipeId: number): Observable<ApiOutput> {
        return this.http.get<HttpResponse<any>>(
            `${this.baseUrl}/recipes/${recipeId}/ingredients`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
    * Gets all user who favourited the recipe
    */
    getAuthorName(accountId: number): Observable<ApiOutput> {
        return this.http.get<HttpResponse<any>>(
            `${this.baseUrl}/accounts/${accountId}/profiles`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }


    // <----------------- Ingredients related routes beginning here ----------------->

    /**
     * Trys to generate an ingredient for a recipe
     */
     newIngredient(newIngredient: Ingredient[], recipeId: number): Observable<ApiOutput> {
        return this.http.post<HttpResponse<any>>(
            `${this.baseUrl}/accounts/recipes/${recipeId}/ingredients`,
            newIngredient, {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }

    /**
     * Trys to generate an ingredient for a recipe
     */
    deleteIngredient(recipeId: number): Observable<ApiOutput> {
        return this.http.delete<HttpResponse<any>>(
            `${this.baseUrl}/accounts/recipes/${recipeId}/ingredients`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }


    /**
     * Trys to get all ingredients existing
     */
    getAllIngredients(): Observable<ApiOutput> {
        return this.http.get<HttpResponse<any>>(
            `${this.baseUrl}/ingredients`,
            {
                observe: 'response'
            }
        ).pipe(
            map((res) => {
                if(!res.body) {
                    return {data: null};
                } else {
                    return {data: (res.body as any)};
                }
            }),
            catchError(this.handleError)
        );
    }
}

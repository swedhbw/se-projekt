import { Injectable } from '@angular/core';
import {FormGroup} from '@angular/forms';

@Injectable({
  providedIn: 'root'
})
export class FormService {
    constructor() { }

    /***
     * Validates the given formGroup
     * @param formGroup
     */
    static validateForm(formGroup: FormGroup): void{
        for (const i in formGroup.controls) {
            if (formGroup.controls.hasOwnProperty(i)) {
                formGroup.controls[i].markAsDirty();
                formGroup.controls[i].updateValueAndValidity();
            }
        }
    }
}

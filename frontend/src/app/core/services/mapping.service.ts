import {Injectable} from '@angular/core';
import {ApiService} from './api.service';
import {Recipe} from '../../shared/models/recipe.model';
import {UtilService} from './util.service';
import {Unit} from '../../shared/models/unit.model';
import {Ingredient} from '../../shared/models/ingredient.model';
import {SelectModel} from '../../shared/models/select.model';

@Injectable({
  providedIn: 'root'
})
export class MappingService {

  constructor(private apiService: ApiService, private utilService: UtilService) { }

  getRecipeById(id: number, callback: Function): void {
    let recipe: any;
    this.apiService.getRecipeById(id).subscribe({
      next: (recipeResult: any) => {
        recipe = recipeResult.data;
        recipe.createdAt = new Date(recipeResult.data.createdAt);
          this.apiService.getIngredientsById(id).subscribe({
              next: (ingredientResult: any) => {

                recipe.ingredients = ingredientResult.data;

                for (let index in recipe.ingredients){
                    recipe.ingredients[index].unit;
                }

                  this.apiService.getAuthorName(recipe.authorID).subscribe({
                    next: (authorResult: any) => {

                      recipe.author = authorResult.data.name;
                      let finaleRecipe: Recipe = recipe;
                      callback(finaleRecipe);
                    },
                    error: (error: any) => {

                        this.utilService.showSnackbarError(error);
                      throw Error(error);
                    }
                });
              },
              error: (error: any) => {

                this.utilService.showSnackbarError(error);
                throw Error(error);
              }
          });
      },
      error: (error: any) => {

        this.utilService.showSnackbarError(error);
        throw Error(error);
      }
  });
  }

    /**
     * Maps a unit to the fitting string
     * @param unit
     */
    mapUnitToString(unit: Unit): String{
      let unitAsString;

      switch (unit) {
          case Unit.MILLILITER:
              unitAsString = 'ml';
              break;
          case Unit.GRAM:
              unitAsString = 'g';
              break;
          case Unit.PIECE:
              unitAsString = 'piece';
              break;
          case Unit.TABLESPOON:
              unitAsString = 'tbsp';
              break;
          case Unit.TEASPOON:
              unitAsString = 'tsp';
              break;
          case Unit.DASH:
              unitAsString = 'dash';
              break;
          default:
              throw Error('No defined unit');
      }

      return unitAsString;
    }

    /**
     * Maps a unit  to the fitting number
     * @param unit
     */
    mapUnitToNumber(unit: Unit): Number{
        let unitAsNumber;

        switch (unit) {
            case Unit.MILLILITER:
                unitAsNumber = 0;
                break;
            case Unit.GRAM:
                unitAsNumber = 1;
                break;
            case Unit.PIECE:
                unitAsNumber = 2;
                break;
            case Unit.TABLESPOON:
                unitAsNumber = 3;
                break;
            case Unit.TEASPOON:
                unitAsNumber = 4;
                break;
            case Unit.DASH:
                unitAsNumber = 5;
                break;
            default:
                throw Error('No defined unit');
        }

        return unitAsNumber;
    }

    /**
     * Converts the given amount in the ingredients from the initialPortions to the targetPortions
     * @param ingredients
     * @param initialPortions
     * @param targetPortions
     */
    convertToPortions(ingredients: Ingredient[], initialPortions: number, targetPortions: number): Ingredient[] {
        let newIngredients: Ingredient[] = [];

        for (const ingredient of ingredients) {
            ingredient.amount = ( ingredient.amount / initialPortions ) * targetPortions;
            ingredient.amount = this._roundTo(ingredient.amount, 2);
            newIngredients.push(ingredient);
        }

        return newIngredients;
    }

    /**
     * Rounds the given number to n places after the comma
     * @param num
     * @param places
     * @private
     */
    private _roundTo(num: number, places: number): number {
        const factor = 10 ** places;
        return Math.round(num * factor) / factor;
    };

    /**
     * Returns the select model for the units
     */
    getUnitAsSelectModel(): SelectModel[]{
        return [
            {display: this.mapUnitToString(Unit.MILLILITER) ,value: Unit.MILLILITER},
            {display: this.mapUnitToString(Unit.GRAM) ,value: Unit.GRAM},
            {display: this.mapUnitToString(Unit.PIECE) ,value: Unit.PIECE},
            {display: this.mapUnitToString(Unit.TABLESPOON) ,value: Unit.TABLESPOON},
            {display: this.mapUnitToString(Unit.TEASPOON) ,value: Unit.TEASPOON},
            {display: this.mapUnitToString(Unit.DASH) ,value: Unit.DASH},
        ];
    }
}

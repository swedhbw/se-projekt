import { CanActivate, Router } from '@angular/router';
import { Injectable } from '@angular/core';
import { AuthService } from './auth.service';
import { UtilService } from './util.service';

@Injectable()
export class AuthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router, private util: UtilService) { }

  canActivate(): boolean {

    // Checks if the user is logged in
    if (this.authService.isLoggedIn()) {

      return true;

    } else {

      //Redirects user to /signup and shows an error
      this.router.navigate(['login']).then(() => {
        this.util.showSnackbar(
          'Permission denied. Redirected to login',
          'danger',
          'No permission',
          'close-outline',
        );
      });
      return false;

    }
  }
}

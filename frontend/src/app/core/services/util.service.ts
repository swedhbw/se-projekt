import { Injectable } from '@angular/core';
import {NbComponentOrCustomStatus, NbGlobalPhysicalPosition, NbIconConfig, NbToastrService} from '@nebular/theme';
import { ApiError } from 'src/app/shared/models/api.model';

@Injectable({
  providedIn: 'root'
})
export class UtilService {
    readonly positions = NbGlobalPhysicalPosition.BOTTOM_LEFT;
    readonly duplicatesBehaviour = 'previous';
    readonly destroyByClick = true;
    innerWidth!: number;
    innerHeight!: number;
    isDarkMode: boolean = false;

    constructor(private snackBar: NbToastrService) {
        this.onResize();
    }

    /**
     * Shows a Snackbar (aka Toastr) for success, failure, basic messages for the user. The configuration is set by the
     * method itself and will be the same over the whole application
     * @param message
     * @param title
     * @param status
     * @param icon
     */
    showSnackbar(
        message: string,
        status?: NbComponentOrCustomStatus,
        title?: string,
        icon?: string | NbIconConfig,
    ): void {
        this.snackBar.show(message, title,{
            status: status,
            duplicatesBehaviour: this.duplicatesBehaviour,
            position: this.positions,
            hasIcon: !!icon,
            icon: icon,
            destroyByClick: this.destroyByClick,
            duration: 5000
        });
    }

    showSnackbarError(error: ApiError): void {
        this.showSnackbar(
            error.message,
            'danger',
            error.title,
            'close-outline',
        );
    }

    /**
     * called on window resize and handles all resize action
     */
    onResize(): void{
        this.innerWidth = window.innerWidth;
        this.innerHeight= window.innerHeight;
    }
}

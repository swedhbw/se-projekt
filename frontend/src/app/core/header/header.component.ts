import {AfterViewInit, Component, ElementRef, OnInit, ViewChild} from '@angular/core';
import {FormBuilder, FormControl, FormGroup, Validators} from '@angular/forms';
import {NbPopoverDirective, NbThemeService, NbTrigger} from '@nebular/theme';
import {FormInputType} from '../../shared/models/form-input-type';
import {FormService} from '../services/form.service';
import {AuthService} from '../services/auth.service';
import {UtilService} from '../services/util.service';
import {Router} from '@angular/router';



@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, AfterViewInit {
    userButtonTitle: string = 'Login';
    loading: boolean = false;

    // Vars for the popover
    loginForm!: FormGroup;
    formInputType = FormInputType;
    popoverTrigger: NbTrigger = NbTrigger.NOOP;
    @ViewChild(NbPopoverDirective) popover!: NbPopoverDirective;
    innerWidth!: number;
    innerHeight!: number;

    @ViewChild('view')
    view!: ElementRef;

    constructor(
        private formBuilder: FormBuilder,
        public authService: AuthService,
        public utilService: UtilService,
        private themeService: NbThemeService,
        private router: Router
    ) { }

    ngOnInit(): void {
        // generate the Form
        this.loginForm = this.formBuilder.group({
            email: [null, [Validators.required, Validators.email]],
            password: [null, [Validators.required]],
        });

        if (localStorage.getItem('darkMode') == 'dark') {
            this.utilService.isDarkMode = true;
            this.changeTheme(false);
        }
    }

    ngAfterViewInit(): void {
        this.onResize();
    }

    // Getters for the controls

    get email(){
        return this.loginForm.get('email') as FormControl;
    }

    get password(){
        return this.loginForm.get('password') as FormControl;
    }

    /**
     * Handles the onclick action for the login from
     */
    onLogin(): void{
        // Validate Form
        FormService.validateForm(this.loginForm);

        // Login if valid
        if(this.loginForm.valid){
            this._startLoading();

            // Send login data to backend and handle events
            this.authService.login(this.email.value, this.password.value)
                .subscribe({
                    next: () => {
                        this._clearForm();
                        this.popover.hide();
                        this.utilService.showSnackbar(
                            'Successfully logged in!',
                            'success',
                            'Logged in',
                            'checkmark-outline',
                        );
                        this.authService.setExpiration();
                        this.router.navigate(['/cook-book']).then(() => this._endLoading());
                    },
                    error: (error) => {
                        this._endLoading();
                        this.utilService.showSnackbar(
                            error.message,
                            'danger',
                            error.title,
                            'close-outline',
                        );
                    }
                });
        } else {
            this.utilService.showSnackbar(
                'Please fill the form correctly!',
                'danger',
                'No valid input',
                'close-outline',
            );
        }
    }

    /**
     * Manage the click on logout
     */
    onLogout(): void{
        this.authService.logout();

        this.utilService.showSnackbar(
            'Successfully logged out!',
            'success',
            'Logged out',
            'checkmark-outline',
        );
    }

    /**
     * Clears the form
     * @private
     */
    private _clearForm(): void {
        this.loginForm.reset();
    }

    /**
     * Toggles the color themes
     */
    changeTheme(toggle: boolean): void {
        if (toggle) {
            this.utilService.isDarkMode = !this.utilService.isDarkMode;
        }
        let themeName;
        console.log(this.utilService.isDarkMode);

        this.utilService.isDarkMode ? themeName = 'dark' : themeName = 'corporate';
        localStorage.setItem('darkMode', this.utilService.isDarkMode ? 'dark' : 'light');

        this.themeService.changeTheme(themeName);
    }


    /**
     * Clears the password
     * @private
     */
    private _clearPassword(): void {
        this.password.reset();
    }

    /**
     * Starts the loading animations
     * @private
     */
    private _startLoading(): void {
        this.loading = true;
        this.loginForm.disable();
    }

    /**
     * End of the loading animation
     * @private
     */
    private _endLoading(): void {
        this.loading = false;
        this.loginForm.enable();
        this._clearPassword();
    }


    /**
     * called on window resize and handles all resize action
     */
     onResize(): void{
        this.innerWidth = this.view.nativeElement.offsetWidth;
        this.innerHeight= this.view.nativeElement.offsetHeight;
    }
}

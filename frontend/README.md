# Frontend of the swe project

##  Folder structure
* **app/** – Angular App
  * **core/** – Service singletons, guards, i18n
    * **api/** – API interaction services
    * **guards/** – routing guards
    * **i18n/** – Language services
    * **services/** – Other services
  * **pages/** – Page-module incl. components, sub-routing
    * **home/** – Home page module
    * **...** – [Example for other page modules]
  * **shared/** – Shared-components, -models, -pipes
    * **animations/** – Animations
    * **components/** – Shared-Components
    * **models/** – Interface definitions (Data-models)
    * **pipes/** – Pipes
  * **app.component.html** – HTML for App-Root
  * **app.component.scss** – Style for App-Root
  * **app.component.spec.ts** – Test for App-Root
  * **app.component.ts** – Typescript for App-Root
  * **app.module.ts** – Root module (**don't change if you don't know what you do**)
  * **app-routing.module.ts** – Root module (**don't change if you don't know what you do**)
* **assets/** – Fonts, Pictures, Icon, general Styles
* **environments/** – Environment variables
* **favicon.ico** – App icon for the browser header
* **index.html** – Root html file (**don't change if you don't know what you do**)
* **main.ts** – Root ts file (**don't change if you don't know what you do**)
* **polyfills.ts** – Root ts file (**don't change if you don't know what you do**)
* **styles.scss** – Root style file (**don't change if you don't know what you do**)
* **test.ts** – Root test file (**don't change if you don't know what you do**)
* **themes.scss** – File for generating on themes in nebular (**don't change if you don't know what you do**)
* **../package.json** – remembers all packages that your app depends on and their versions
* **../angular.json** – remembers all angular settings that your app depends on and their versions

Ref: [Lukas Pfaffen - Angular Vorelsung DHBW WI SE 2021]

### Commands:
* `ng serve` – Dev server on `http://localhost:4200/`
* `ng generate component path-from-app-to-component/component-name` – Generate a new component You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`
* `ng g m pages/module-name --route route/to/page --module app` – Generate a new page and add routing to this page (**recommended for new pages**)
* `ng build` – Deploy the app
* `ng test` – Execute the unit tests via [Karma](https://karma-runner.github.io).


## Environment files
The Environment file **"src/environments/environment.ts"** contains all customizable variables to run the app properly (must be manually created).

**Hint:**
* *environment.ts -> develop*
* *environment.prod.ts -> production*


[Prisma]: <https://www.prisma.io/>
[connection string]: <https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/relational-databases/connect-your-database-typescript-postgres>
[migrate]: <https://www.prisma.io/docs/concepts/components/prisma-migrate>
[generates prisma client]: <https://www.prisma.io/docs/concepts/components/prisma-client>

## ESLint
Run `ng lint --fix` for linitng


## Further help
To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI Overview and Command Reference](https://angular.io/cli) page.

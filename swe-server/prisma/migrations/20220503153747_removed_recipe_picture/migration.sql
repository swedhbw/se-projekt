/*
  Warnings:

  - You are about to drop the column `picture` on the `Recipe` table. All the data in the column will be lost.

*/
-- AlterTable
ALTER TABLE `Recipe` DROP COLUMN `picture`;

import path from 'path';
import * as fs from 'fs/promises';
import { extensions } from '../models/extensions.model';
import { HttpError } from '../models/http-error.model';
import { fail, Result, success } from '../models/result.model';

const IMAGE_FOLDER = '../../images/';

export enum ImageType {
    RecipeShowcase
}

/**
 * Uploads an image to the server.
 *
 * @param image image to be uploaded
 * @param imageType type of the image
 * @param name name of the image
 * @returns uploaded image
 */
export async function saveImage (image: any, imageType: ImageType, name: string): Promise<Result<any>> {
    const extension = image.name.substr(image.name.lastIndexOf('.'));
    const folder = getImageFolder(imageType);

    if (extensions.includes(extension)) {
        try {
            // save the picture at the given path
            await image.mv(path.join(__dirname, folder + name + extension));
            return success(image);
        } catch (err) {
            return fail(new HttpError('Failed to save image', 500));
        }
    } else {
        return fail(new HttpError('Wrong image format', 400));
    }
}

export async function getImagePath (imageType: ImageType, name: string): Promise<Result<string>> {
    const folder = getImageFolder(imageType);

    try {
        const fileNames = await fs.readdir(path.join(__dirname, folder));
        for (const fileName of fileNames) {
            if (fileName.split('.')[0] === name) {
                return success(path.join(__dirname, folder + fileName));
            }
        }

        return fail(new HttpError('Image not found', 404));
    } catch (err) {
        return fail(new HttpError('Failed to get image', 500));
    }
}

export async function deleteImage (imageType: ImageType, name: string): Promise<Result<void>> {
    const folder = getImageFolder(imageType);

    try {
        const fileNames = await fs.readdir(path.join(__dirname, folder));

        for (const fileName of fileNames) {
            if (fileName.includes(name)) {
                await fs.rm(path.join(__dirname, folder + fileName));

                return success(undefined);
            }
        }

        return fail(new HttpError('Image not found', 404));
    } catch (err) {
        return fail(new HttpError('Failed to remove image', 500));
    }
}

export async function checkForExistingImage (imageType: ImageType, name: string): Promise<Result<boolean>> {
    const folder = getImageFolder(imageType);

    try {
        const fileNames = await fs.readdir(path.join(__dirname, folder));

        for (const fileName of fileNames) {
            if (fileName.includes(name)) {
                return success(true);
            }
        }

        return success(false);
    } catch (err) {
        return fail(new HttpError('Failed to check for existing image. Please try again', 500));
    }
}

export function getImageFolder (imageType: ImageType): string {
    switch (imageType) {
    case ImageType.RecipeShowcase:
        return IMAGE_FOLDER + 'recipes/';
    }
}

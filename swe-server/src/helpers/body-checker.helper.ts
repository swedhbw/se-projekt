/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * Check if the given elements are all of the type number
 * @param elements array of elements
 * @returns result as boolean
 */
export function areNumbers (elements: any[]): boolean {
    for (const element of elements) {
        // the given element is not a number if it is undefined, null or cant be converted to a number.
        if ((!element || isNaN(+element)) && element !== 0) return false;
    }
    return true;
}

/**
 * Check if the elements are not null, undefined or an empty string
 * @param elements array of elements
 * @returns result as boolean
 */
export function areNotNullOrEmpty (elements: any[]): boolean {
    for (const element of elements) {
        if (element === undefined || element === null || element === '') return false;
    }
    return true;
}

/**
 * Check if the elements are not null or undefined. But the elements can be an empty string
 * @param elements array of elements
 * @returns result as boolean
 */
export function areNotNullButEmpty (elements: any[]): boolean {
    for (const element of elements) {
        if (element !== '' && (element === undefined || element === null)) return false;
    }
    return true;
}

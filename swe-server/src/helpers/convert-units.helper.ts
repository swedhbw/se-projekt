import { Unit } from '../models/unit.model';

export function convert (value: number, fromUnit: Unit | undefined, toUnit: number) {
    let converted = value;

    if (fromUnit !== undefined) {
        if (fromUnit === Unit.MILLILITER) {
            switch (toUnit) {
            case Unit.MILLILITER:
                break;
            case Unit.GRAM:
                break;
            case Unit.PIECE:
                converted = (value / 50);
                break;
            case Unit.TABLESPOON:
                converted = (value / 15);
                break;
            case Unit.TEASPOON:
                converted = (value / 5);
                break;
            case Unit.DASH:
                converted = (value * 1.5);
                break;
            default:
                break;
            }
        } else if (fromUnit === Unit.GRAM) {
            switch (toUnit) {
            case Unit.MILLILITER:
                break;
            case Unit.GRAM:
                break;
            case Unit.PIECE:
                converted = (value / 20);
                break;
            case Unit.TABLESPOON:
                converted = (value / 15);
                break;
            case Unit.TEASPOON:
                converted = (value / 5);
                break;
            case Unit.DASH:
                converted = (value * 1.5);
                break;
            default:
                break;
            }
        } else if (fromUnit === Unit.PIECE) {
            switch (toUnit) {
            case Unit.MILLILITER:
                converted = (value * 50);
                break;
            case Unit.GRAM:
                converted = (value * 20);
                break;
            case Unit.PIECE:
                break;
            case Unit.TABLESPOON:
                converted = (value * 15);
                break;
            case Unit.TEASPOON:
                converted = (value * 45);
                break;
            case Unit.DASH:
                converted = (value * 200);
                break;
            default:
                break;
            }
        } else if (fromUnit === Unit.TABLESPOON) {
            switch (toUnit) {
            case Unit.MILLILITER:
                converted = (value * 15);
                break;
            case Unit.GRAM:
                converted = (value * 15);
                break;
            case Unit.PIECE:
                converted = (value / 15);
                break;
            case Unit.TABLESPOON:
                break;
            case Unit.TEASPOON:
                converted = (value * 3);
                break;
            case Unit.DASH:
                converted = (value * 24);
                break;
            default:
                break;
            }
        } else if (fromUnit === Unit.TEASPOON) {
            switch (toUnit) {
            case Unit.MILLILITER:
                converted = (value * 5);
                break;
            case Unit.GRAM:
                converted = (value * 5);
                break;
            case Unit.PIECE:
                converted = (value / 45);
                break;
            case Unit.TABLESPOON:
                converted = (value / 3);
                break;
            case Unit.TEASPOON:
                break;
            case Unit.DASH:
                converted = (value * 8);
                break;
            default:
                break;
            }
        } else if (fromUnit === Unit.DASH) {
            switch (toUnit) {
            case Unit.MILLILITER:
                converted = (value / 1.5);
                break;
            case Unit.GRAM:
                converted = (value / 1.5);
                break;
            case Unit.PIECE:
                converted = (value / 200);
                break;
            case Unit.TABLESPOON:
                converted = (value / 24);
                break;
            case Unit.TEASPOON:
                converted = (value / 8);
                break;
            case Unit.DASH:
                break;
            default:
                break;
            }
        }
    }
    return converted;
}

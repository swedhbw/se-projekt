import { SearchIngredient } from '../models/search-ingredient.model';

/**
 * makes string lowercase and removes unwanted spaces
 * @param name
 * @returns {string}
 */
export function normaliseIngredientName (name: string): string {
    let returnString = name.toLowerCase();
    returnString = returnString.trim();
    returnString = returnString.replace(/[ ]{2,}/g, ' ');
    return returnString;
}

// default ingredients (name should already be normalized)
export const additionalIngredients: Array<SearchIngredient> = [
    {
        name: 'salt',
        amount: 1,
        unit: 5
    },
    {
        name: 'pepper',
        amount: 1,
        unit: 5
    },
    {
        name: 'sugar',
        amount: 1,
        unit: 5
    },
    {
        name: 'oil',
        amount: 1,
        unit: 3
    }
];

// Model to get account information's of a request

export class RequestAccount {
    public accountId: string;

    constructor (accountId: string) {
        this.accountId = accountId;
    }
}

import { Recipe } from './recipe.model';
import { Recipe as PrismaRecipe } from '@prisma/client';

// model to simulate a Map, as a Map cannot be copied to another map and then altered/deleted -> reference, not value
export class RecipeMap {
    keys: Array<number>;
    values: Array<Recipe[]>;
    length: number;

    constructor (map: Map<number, Array<PrismaRecipe>>) {
        this.keys = Array.from(map.keys());
        this.length = this.keys.length;

        this.values = [];

        for (let i = 0; i < this.length; i++) {
            this.values.push([]);
        }

        for (const entry of map.entries()) {
            for (let i = 0; i < entry[1].length; i++) {
                this.values[entry[0]].push(new Recipe(
                    entry[1][i].id,
                    entry[1][i].name,
                    entry[1][i].instructions,
                    entry[1][i].authorID,
                    entry[1][i].isPublic,
                    entry[1][i].createdAt,
                    entry[1][i].totalTime,
                    entry[1][i].prepTime,
                    entry[1][i].difficulty,
                    entry[1][i].portions
                ));
            }
        }
    }
}

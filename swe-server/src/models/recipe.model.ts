import { checkForFavorite } from '../providers/favorite.provider';
import { Difficulty } from './difficulty.model';
import { fail, Result, success } from './result.model';

export class Recipe {
    id: number;
    name: string;
    instructions: string;
    authorID: string;
    isPublic: boolean;
    createdAt: Date;
    totalTime: number;
    prepTime: number;
    difficulty: Difficulty;
    portions: number;
    isFavorite?: boolean;

    constructor (
        id: number,
        name: string,
        instructions: string,
        authorID: string,
        isPublic: boolean,
        createdAt: Date,
        totalTime: number,
        prepTime: number,
        difficulty: Difficulty,
        portions: number,
        isFavorite?: boolean
    ) {
        this.id = id;
        this.name = name;
        this.instructions = instructions;
        this.authorID = authorID;
        this.isPublic = isPublic;
        this.createdAt = createdAt;
        this.totalTime = totalTime;
        this.prepTime = prepTime;
        this.difficulty = difficulty;
        this.portions = portions;
        this.isFavorite = isFavorite;
    }

    public async setFavorite (accountId: string): Promise<Result<boolean>> {
        const isFavorite = await checkForFavorite(this.id, accountId);

        if (isFavorite.isSucc()) {
            this.isFavorite = isFavorite.value;

            return success(isFavorite.value);
        } else return fail(isFavorite.value);
    }
}

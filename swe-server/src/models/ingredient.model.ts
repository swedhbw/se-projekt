import { Unit } from './unit.model';

export class Ingredient {
    recipeID: number;
    name: string;
    amount: number;
    unit: Unit;

    constructor (
        recipeID: number,
        name: string,
        amount: number,
        unit: Unit
    ) {
        this.recipeID = recipeID;
        this.name = name;
        this.amount = amount;
        this.unit = unit;
    }
}

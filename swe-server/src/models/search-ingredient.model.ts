import { Unit } from './unit.model';

export class SearchIngredient {
    name: string;
    amount: number;
    unit: Unit;

    constructor (
        name: string,
        amount: number,
        unit: Unit
    ) {
        this.name = name;
        this.amount = amount;
        this.unit = unit;
    }
}

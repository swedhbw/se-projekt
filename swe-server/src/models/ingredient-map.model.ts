import { Ingredient } from './ingredient.model';

// model to simulate a Map, as a Map cannot be copied to another map and then altered/deleted -> reference, not value
export class IngredientMap {
    keys: Array<number>;
    values: Array<Ingredient[]>;
    length: number;

    constructor (map: Map<number, Array<Ingredient>>) {
        this.keys = Array.from(map.keys());
        this.values = Array.from(map.values());
        this.length = this.keys.length;
    }
}

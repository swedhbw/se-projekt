// same as unitmodel in frontend
export enum Unit {
    MILLILITER = 0,
    GRAM = 1,
    PIECE = 2,
    TABLESPOON = 3,
    TEASPOON = 4,
    DASH = 5
}

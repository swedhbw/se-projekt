import { Request } from 'express';

// Add recipe id to express request parameters
export type RecipeRequest = Request & {
    params: {
        recipeId: string;
    };
};

import { Request } from 'express';

// Add recipe id to express request parameters
export type AccountRequest = Request & {
    params: {
        accountId: string;
    };
};

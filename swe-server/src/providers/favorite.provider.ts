import 'dotenv';
import { Favorites, PrismaClient } from '@prisma/client';
import { HttpError } from '../models/http-error.model';
import { fail, Result, success } from '../models/result.model';

const prisma = new PrismaClient();

/**
 * Create a new favorite
 * @param accountId id of the account
 * @param recipeId id of the recipe
 * @returns model of the inserted favorite
 */
export async function createFavorite (accountId: string, recipeId: number): Promise<Result<Favorites>> {
    try {
        const createdFavorite = await prisma.favorites.create({
            data: {
                accountID: accountId,
                recipeID: recipeId
            }
        });

        return success(createdFavorite);
    } catch (err) {
        return fail(new HttpError('Failed to create favorites', 500));
    }
}

/**
 * Get all favorites of one account
 * @returns array of all favorites
 * @param accountId id of the account
 */
export async function getFavoritesByAccount (accountId: string): Promise<Result<Array<Favorites>>> {
    try {
        const favoritesFromDB = await prisma.favorites.findMany({
            where: {
                accountID: accountId
            }
        });

        if (favoritesFromDB) return success(favoritesFromDB);
        else return fail(new HttpError('Account does not exist', 404));
    } catch (err) {
        return fail(new HttpError('Failed to get account', 500));
    }
}

/**
 * Delete favorite
 *
 * @param accountId universally unique identifier of the account
 * @param recipeId universally unique identifier of the recipe
 * @returns deleted account model
 */
export async function deleteFavorite (accountId: string, recipeId: number): Promise<Result<Favorites>> {
    try {
        const result = await prisma.favorites.delete({
            where: {
                accountID_recipeID: {
                    accountID: accountId,
                    recipeID: recipeId
                }
            }
        });

        return success(result);
    } catch (err) {
        return fail(new HttpError('Failed to delete account', 500));
    }
}

export async function checkForFavorite (recipeId: number, authorID: string): Promise<Result<boolean>> {
    try {
        const favCount = await prisma.favorites.count({
            where: {
                recipeID: recipeId,
                accountID: authorID
            }
        });

        if (favCount > 0) return success(true);
        else return success(false);
    } catch (err) {
        return fail(new HttpError('Failed to check for author', 500));
    }
}

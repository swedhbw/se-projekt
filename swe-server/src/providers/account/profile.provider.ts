import { PrismaClient } from '@prisma/client';
import { HttpError } from '../../models/http-error.model';
import { Profile } from '../../models/account/profile.model';
import { fail, Result, success } from '../../models/result.model';

const prisma = new PrismaClient();

/**
 * Get profile of account
 *
 * @param accountId universally unique identifier of the account
 * @returns profile model of the account
 */
export async function getProfile (accountId: string): Promise<Result<Profile>> {
    try {
        const account = await prisma.account.findUnique({
            where: {
                id: accountId
            },
            select: {
                id: true,
                name: true
            }
        });

        if (account) return success(new Profile(account.id, account.name));
        else return fail(new HttpError('Account does not exist', 404));
    } catch (err) {
        return fail(new HttpError('Failed to get profile', 500));
    }
}

import { Favorites, Prisma, PrismaClient, Recipe as PrismaRecipe } from '@prisma/client';
import { Difficulty } from '../models/difficulty.model';
import { HttpError } from '../models/http-error.model';
import { Ingredient } from '../models/ingredient.model';
import { SearchIngredient } from '../models/search-ingredient.model';
import { fail, Result, success } from '../models/result.model';
import { Recipe } from '../models/recipe.model';
import { RecipeMap } from '../models/recipe-map.model';
import { IngredientMap } from '../models/ingredient-map.model';
import { convert } from '../helpers/convert-units.helper';
import { Unit } from '../models/unit.model';
import { normaliseIngredientName, additionalIngredients } from '../helpers/ingredient.helper';

const prisma = new PrismaClient();

/**
 * Add a new recipe to the database
 * @param title fitting title for the recipe
 * @param instructions instruction of the given recipe
 * @returns model of the inserted recipe
 */
export async function createRecipe (
    name: string,
    instructions: string,
    authorId: string,
    isPublic: boolean,
    totalTime: number,
    prepTime: number,
    difficulty: Difficulty,
    portions: number
): Promise<Result<Recipe>> {
    try {
        const createdRecipe = await prisma.recipe.create({
            data: {
                name: name,
                instructions: instructions,
                authorID: authorId,
                isPublic: isPublic,
                totalTime: totalTime,
                prepTime: prepTime,
                difficulty: difficulty.valueOf(),
                portions: portions
            }
        });

        return success(new Recipe(
            createdRecipe.id,
            createdRecipe.name,
            createdRecipe.instructions,
            createdRecipe.authorID,
            createdRecipe.isPublic,
            createdRecipe.createdAt,
            createdRecipe.totalTime,
            createdRecipe.prepTime,
            createdRecipe.difficulty,
            createdRecipe.portions
        ));
    } catch (err) {
        return fail(new HttpError('Failed to create recipe', 500));
    }
}

/**
 * Get all recipes of the database, that either:
 * - match the ingredients
 * - require x more ingredient or require more from x ingredient
 * @returns recipeMap
 */
export async function queryRecipes (ingredients: Array<SearchIngredient>, fromAccountId: string | null, portions: number, loggedInAccountId: string | null): Promise<Result<RecipeMap>> {
    try {
        if (ingredients.length === 0) return fail(new HttpError('No ingredients given', 400));

        const ingredientsList: Array<string> = [];

        for (let i = 0; i < ingredients.length; i++) {
            ingredientsList.push(normaliseIngredientName(ingredients[i].name));
        }

        // add additional ingredients (default ones) from the ingredients in the ingredient.helper.ts
        for (const entry of additionalIngredients) {
            if (ingredientsList.indexOf(entry.name) === -1) {
                ingredientsList.push(entry.name);

                ingredients.push({
                    name: entry.name,
                    amount: entry.amount,
                    unit: entry.unit
                });
            }
        }

        // number to hold the bonus ingredients to be found
        const bonusIngredients = 2;
        // array to hold the found Ingredeints of tbhe database query
        const foundIngredients = [];

        let accountRecipesIds: null | Array<number> = null;

        // get all Recipes of a given user, used when filtering
        if (fromAccountId !== null) {
            accountRecipesIds = ((await getRecipesByAuthor(fromAccountId)).value as Recipe[]).map(recipe => recipe.id);
        }

        for (let i = 0; i <= bonusIngredients; i++) {
            foundIngredients.push(await prisma.$queryRaw<Ingredient[]>`
            SELECT *
            FROM Ingredient
            WHERE recipeID IN
                  (SELECT recipeID
                   FROM Ingredient i
                   WHERE name IN (${Prisma.join(ingredientsList)})
                   GROUP BY recipeID
                   HAVING COUNT(recipeID) =
                          (SELECT COUNT(recipeID)
                           FROM Ingredient v
                           WHERE i.recipeID = v.recipeID) - ${i})
            ORDER BY recipeID, amount`);
        }

        // contains all ingredients (also +x Ingredients)
        const allIngredients = new Map<number, IngredientMap>();
        // contains all ingredients of either 0, +1 or +2 (recipes)
        const formattedIngredients = new Map<number, Array<Ingredient>>();
        // helps in getting the ingredients for formattedIngredients
        let helpArray: Array<Ingredient> = [];
        // checks, when a switch happens -> push the helpArray
        let recipeID = 0;

        if (foundIngredients.length === 0) {
            return success(new RecipeMap(new Map()));
        }

        // get all Ingredients of an ID and append it to the Helparray. When a new id comes, push the array in the formatted one
        for (let j = 0; j < foundIngredients.length; j++) {
            if (foundIngredients[j].length === 0) {
                continue;
            } else {
                recipeID = foundIngredients[j][0].recipeID;
            }

            for (let i = 0; i < foundIngredients[j].length; i++) {
                if (foundIngredients[j][i].recipeID !== recipeID) {
                    formattedIngredients.set(recipeID, helpArray);

                    helpArray = [];
                    recipeID = foundIngredients[j][i].recipeID;
                }
                helpArray.push(foundIngredients[j][i]);
            }

            formattedIngredients.set(recipeID, helpArray);

            allIngredients.set(j, new IngredientMap(formattedIngredients));

            helpArray = [];
            formattedIngredients.clear();
        }

        // stores, if a recipe is valid (all amounts are less or equal to the userinput)
        let validRecipe = 0;
        // the to be compared data
        let comparisonAmount: number | undefined = 0;
        let comparisonObject: SearchIngredient | undefined;
        let comparisonUnit: Unit | undefined = 0;
        // the remaining recipes that are valid by amount
        const filteredRecipes: Map<number, Array<PrismaRecipe>> = new Map();
        // Array to store the recipes under +1 and +2
        const recipeArray: Array<Array<PrismaRecipe>> = [];
        // index where to place the recipe
        let index = 0;
        // recipe from the database
        let recipeAppend: PrismaRecipe | null;
        // store the recipeID along with it's portions
        let recipeAmount: PrismaRecipe | null;

        for (let i = 0; i <= bonusIngredients; i++) {
            recipeArray.push([]);
        }

        // iterate over the formatted array and check for each ingredient, if it is valid. if one is invalid, it will not be put in the solution
        for (const entry of allIngredients.entries()) {
            // iterate over the ingredients of a given amount (0, +1, +2, ...)
            for (let i = 0; i < entry[1].length; i++) {
                // get the portions of the recipe
                recipeAmount = await prisma.recipe.findUnique({
                    where: {
                        id: entry[1].keys[i]
                    }
                });

                // iterate over the single ingredients per recipeID
                for (let j = 0; j < entry[1].values[i].length; j++) {
                    // comparisonamount: Amount given by user
                    comparisonObject = ingredients.find(e => normaliseIngredientName(e.name) === entry[1].values[i][j].name);

                    if (comparisonObject !== undefined && recipeAmount?.portions !== undefined) {
                        comparisonObject.name = normaliseIngredientName(comparisonObject?.name);
                        comparisonAmount = comparisonObject?.amount;
                        comparisonUnit = comparisonObject?.unit;

                        // increments, if the user has less than required by recipe
                        if ((entry[1].values[i][j].amount / recipeAmount?.portions) > (convert(comparisonAmount, comparisonUnit, entry[1].values[i][j].unit) / portions)) {
                            validRecipe++;
                        }
                    }
                }

                // determines the final position (+x) of a recipe. If it requires more of an ingredient, that the user has, it will increment due to validRecipe
                index = validRecipe + entry[0];

                if (index <= bonusIngredients) {
                    // find the recipe in the database
                    recipeAppend = await prisma.recipe.findUnique({
                        where: {
                            id: entry[1].keys[i]
                        }
                    });

                    if (recipeAppend) {
                        if ((!recipeAppend.isPublic && (loggedInAccountId !== null && recipeAppend.authorID === loggedInAccountId)) || recipeAppend.isPublic) {
                            // if filtering by user
                            if (fromAccountId !== null) {
                                if (accountRecipesIds?.includes(recipeAppend.id)) {
                                    recipeArray[index].push(recipeAppend);
                                }
                            } else {
                                recipeArray[index].push(recipeAppend);
                            }
                        }
                    }
                }
                validRecipe = 0;
            }
        }

        for (let i = 0; i < recipeArray.length; i++) {
            filteredRecipes.set(i, recipeArray[i]);
        }

        // map is written in own object, due to Map copy being by reference
        return success(new RecipeMap(filteredRecipes));
        // return success({ 1: recipeArray[0], 2: recipeArray[1], 3: recipeArray[2] });
    } catch (err) {
        console.log(err);
        return fail(new HttpError('Failed to get recipes', 500));
    }
}

/**
 * Get recipe by it's id
 * @param id universally unique identifier of the recipe
 * @returns recipe model
 */
export async function getRecipeById (id: number): Promise<Result<Recipe>> {
    try {
        const recipe = await prisma.recipe.findUnique({
            where: {
                id: id
            }
        });

        if (recipe) {
            const newRecipes = new Recipe(
                recipe.id,
                recipe.name,
                recipe.instructions,
                recipe.authorID,
                recipe.isPublic,
                recipe.createdAt,
                recipe.totalTime,
                recipe.prepTime,
                recipe.difficulty,
                recipe.portions
            );
            return success(newRecipes);
        } else return fail(new HttpError('Recipe does not exist', 404));
    } catch (err) {
        return fail(new HttpError('Failed to get recipe', 500));
    }
}

/**
 * Get all recipes of an account
 *
 * @param author unique identifier of the account
 * @returns all recipes of the account
 */
export async function getRecipesByAuthor (author: string): Promise<Result<Array<Recipe>>> {
    try {
        const recipes = await prisma.recipe.findMany({
            where: {
                authorID: author
            }
        });

        const newRecipes = recipes.map(recipe => new Recipe(
            recipe.id,
            recipe.name,
            recipe.instructions,
            recipe.authorID,
            recipe.isPublic,
            recipe.createdAt,
            recipe.totalTime,
            recipe.prepTime,
            recipe.difficulty,
            recipe.portions
        ));

        return success(newRecipes);
    } catch (err) {
        return fail(new HttpError('Failed to get recipes', 500));
    }
}

/**
 * Patch recipe by it's id
 * @param id universally unique identifier of the recipe
 * @returns recipe model
 */
export async function patchRecipe (
    recipeId: number,
    name: string,
    instructions: string,
    isPublic: boolean,
    totalTime: number,
    prepTime: number,
    difficulty: Difficulty,
    portions: number
): Promise<Result<Recipe>> {
    try {
        const recipe = await prisma.recipe.update({
            where: {
                id: recipeId
            },
            data: {
                name: name,
                instructions: instructions,
                isPublic: isPublic,
                totalTime: totalTime,
                prepTime: prepTime,
                difficulty: difficulty.valueOf(),
                portions: portions
            }
        });

        if (recipe) {
            const recipeWithFalseFavorite = new Recipe(
                recipe.id,
                recipe.name,
                recipe.instructions,
                recipe.authorID,
                recipe.isPublic,
                recipe.createdAt,
                recipe.totalTime,
                recipe.prepTime,
                recipe.difficulty,
                recipe.portions
            );
            return success(recipeWithFalseFavorite);
        } else return fail(new HttpError('Update failed', 404));
    } catch (err) {
        return fail(new HttpError('Failed to get recipe', 500));
    }
}

/**
 * Delete recipe by it's id
 * @param id universally unique identifier of the recipe
 * @returns recipe model
 */
export async function deleteRecipe (id: number): Promise<Result<Recipe>> {
    try {
        const recipe = await prisma.recipe.delete({
            where: {
                id: id
            }
        });
        if (recipe) {
            const recipeWithFalseFavorite = new Recipe(
                recipe.id,
                recipe.name,
                recipe.instructions,
                recipe.authorID,
                recipe.isPublic,
                recipe.createdAt,
                recipe.totalTime,
                recipe.prepTime,
                recipe.difficulty,
                recipe.portions
            );
            return success(recipeWithFalseFavorite);
        } else return fail(new HttpError('Delete failed', 404));
    } catch (err) {
        return fail(new HttpError('Failed to delete recipe', 500));
    }
}

/**
 * Check if account is the author of the recipe
 *
 * @param recipeId unique identifier of the recipe
 * @param authorID unique identifier of the author
 * @returns true if the account is the author of the recipe
 */
export async function checkForAuthor (recipeId: number, authorID: string): Promise<Result<boolean>> {
    try {
        const recipe = await prisma.recipe.findUnique({
            where: {
                id: recipeId
            }
        });

        if (recipe) {
            if (recipe.authorID === authorID) return success(true);
            else return success(false);
        } else return fail(new HttpError('Recipe does not exist', 404));
    } catch (err) {
        return fail(new HttpError('Failed to check for author', 500));
    }
}

/**
 * Get all favorite recipes of the recipe
 * @returns array of all favorite
 * @param recipeId universally unique identifier of the recipe
 */
export async function getFavoriteRecipes (recipeId: number): Promise<Result<Array<Favorites>>> {
    try {
        const favorites = await prisma.favorites.findMany({
            where: {
                recipeID: recipeId
            }
        });

        return success(favorites);
    } catch (err) {
        return fail(new HttpError('Failed to get all favorite recipes', 500));
    }
}

import { Prisma, PrismaClient } from '@prisma/client';
import { HttpError } from '../models/http-error.model';
import { Ingredient } from '../models/ingredient.model';
import { fail, Result, success } from '../models/result.model';
import { normaliseIngredientName } from '../helpers/ingredient.helper';

const prisma = new PrismaClient();

/**
 * Add ingredient to recipe
 *
 * @param recipeId id of the corresponding recipe
 * @param ingredients array of ingredients to add with (name, amount and unit)
 * @returns ingredient object
 */
export async function addIngredients (recipeId: number, ingredients: Array<{name: string, amount: number, unit: number}>): Promise<Result<Array<Ingredient>>> {
    try {
        const resultIngredients = new Array<Ingredient>();

        for (const { name, amount, unit } of ingredients) {
            const ingredient = await prisma.ingredient.create({
                data: {
                    recipeID: recipeId,
                    name: normaliseIngredientName(name),
                    amount,
                    unit
                }
            });

            resultIngredients.push(new Ingredient(ingredient.recipeID, ingredient.name, ingredient.amount, ingredient.unit));
        }

        return success(resultIngredients);
    } catch (error) {
        if (error instanceof Prisma.PrismaClientKnownRequestError && error.code === 'P2015') {
            return fail(new HttpError('Recipe with given ID does not exist', 404));
        } else return fail(new HttpError('Failed to add ingredient', 500));
    }
}

/**
 * Get all ingredients of a recipe
 *
 * @param recipeId id of the corresponding recipe
 * @returns all ingredients of the recipe as an array
 */
export async function getAllIngredientsOfRecipe (recipeId: number): Promise<Result<Ingredient[]>> {
    try {
        const ingredients = await prisma.ingredient.findMany({
            where: {
                recipeID: recipeId
            }
        });

        return success(ingredients);
    } catch (error) {
        return fail(new HttpError('Failure to get ingredients', 500));
    }
}

// Get all ingredients that exist in the database without duplicates
export async function getAllIngredients (): Promise<Result<string[]>> {
    try {
        const ingredients = await prisma.$queryRaw<Ingredient[]>`
        SELECT *, count(*) as count 
        FROM Ingredient 
        GROUP BY name 
        ORDER BY count DESC`;

        return success(ingredients.map(ingredient => ingredient.name));
    } catch (error) {
        return fail(new HttpError('Failure to get ingredients', 500));
    }
}

/**
 * Get one ingredient of a recipe by it's id
 *
 * @param recipeId id of the corresponding recipe
 * @param name name of the ingredient
 * @returns ingredient object
 */
export async function getIngredient (recipeId: number, name: string): Promise<Result<Ingredient>> {
    try {
        const ingredient = await prisma.ingredient.findFirst({
            where: {
                recipeID: recipeId,
                name
            }
        });

        if (ingredient) return success(ingredient);
        else return fail(new HttpError('Ingredient of recipe not found', 404));
    } catch (error) {
        return fail(new HttpError('Failure to get ingredient', 500));
    }
}

/**
 * Remove ingredient from recipe
 *
 * @param recipeId id of the corresponding recipe
 * @param name name of the ingredient
 * @returns deleted ingredient object
 */
export async function removeIngredient (recipeId: number, name: string): Promise<Result<Ingredient>> {
    try {
        const ingredient = await prisma.ingredient.delete({
            where: {
                recipeID_name: {
                    recipeID: recipeId,
                    name
                }
            }
        });

        return success(ingredient);
    } catch (error) {
        if (error instanceof Prisma.PrismaClientKnownRequestError && error.code === 'P2025') {
            return fail(new HttpError('Recipe with this ingredient does not exist', 404));
        } else return fail(new HttpError('Failed to remove ingredient', 500));
    }
}

/**
 * Remove all ingredients of a recipe
 *
 * @param recipeId id of the corresponding recipe
 * @returns deleted ingredients
 */
export async function removeAllIngredients (recipeId: number): Promise<Result<void>> {
    try {
        await prisma.ingredient.deleteMany({
            where: {
                recipeID: recipeId
            }
        });

        return success(undefined);
    } catch (error) {
        return fail(new HttpError('Failure to remove ingredients', 500));
    }
}

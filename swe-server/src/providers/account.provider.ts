import 'dotenv';
import { PrismaClient } from '@prisma/client';
import bcrypt from 'bcrypt';
import { Account } from '../models/account.model';
import { HttpError } from '../models/http-error.model';
import { fail, Result, success } from '../models/result.model';
import { PrismaClientKnownRequestError } from '@prisma/client/runtime';

const prisma = new PrismaClient();

/**
 * Create new account
 * @param name name of the user account
 * @param email email of the user
 * @param plainPassword not encrypted password of the account
 * @returns created account model
 */
export async function createAccount (name: string, email: string, plainPassword: string): Promise<Result<Account>> {
    try {
        const encryptedPassword = await bcrypt.hash(
            plainPassword,
            isNaN(Number(process.env.ENCRYPT_SALT)) ? 10 : Number(process.env.ENCRYPT_SALT)
        );

        const account = await prisma.account.create({
            data: {
                name: name,
                email: email,
                password: encryptedPassword
            }
        });

        return success(new Account(account.id, account.name, account.email));
    } catch (err) {
        if (err instanceof PrismaClientKnownRequestError && err.code === 'P2002') {
            return fail(new HttpError('This user already exists', 404));
        } else { return fail(new HttpError('Failed to create account', 500)); }
    }
}

/**
 * Get account by it's id
 * @param id universally unique identifier of the account
 * @returns account model
 */
export async function getAccountById (id: string): Promise<Result<Account>> {
    try {
        const account = await prisma.account.findUnique({
            where: {
                id: id
            },
            select: {
                id: true,
                name: true,
                email: true
            }
        });

        if (account) return success(new Account(account.id, account.name, account.email));
        else return fail(new HttpError('Account does not exist', 404));
    } catch (err) {
        return fail(new HttpError('Failed to get account', 500));
    }
}

/**
 * Get account by it's email
 *
 * @param email email of the account
 * @returns account model
 */
export async function getAccountByEmail (email: string): Promise<Result<Account>> {
    try {
        const account = await prisma.account.findUnique({
            where: {
                email: email
            },
            select: {
                id: true,
                name: true,
                email: true
            }
        });

        if (account) return success(new Account(account.id, account.name, account.email));
        else return fail(new HttpError('Account does not exist', 404));
    } catch (err) {
        return fail(new HttpError('Failed to get account', 500));
    }
}

/**
 * Update account information's
 *
 * @param account new Account model
 * @returns new account model
 */
export async function updateAccount (account: Account): Promise<Result<Account>> {
    try {
        const updatedAccount = await prisma.account.update({
            where: {
                id: account.id
            },
            data: {
                name: account.name,
                email: account.email
            }
        });

        return success(new Account(updatedAccount.id, updatedAccount.name, updatedAccount.email));
    } catch (err) {
        return fail(new HttpError('Failed to update account', 500));
    }
}

/**
 * Delete account by it's id
 *
 * @param id universally unique identifier of the account
 * @returns deleted account model
 */
export async function deleteAccount (id: string): Promise<Result<Account>> {
    try {
        const account = await prisma.account.delete({
            where: {
                id: id
            }
        });

        return success(new Account(account.id, account.name, account.email));
    } catch (err) {
        return fail(new HttpError('Failed to delete account', 500));
    }
}

/**
 * Update account password
 *
 * @param id universally unique identifier of the account
 * @param newPassword new password of the account
 */
export async function updateAccountPassword (id: string, currPassword: string, newPassword: string): Promise<Result<void>> {
    try {
        // get current account model
        const account = await prisma.account.findUnique({
            where: {
                id: id
            },
            select: {
                password: true
            }
        });

        if (account) {
            // compare password
            const authResult = await bcrypt.compare(currPassword, account.password);
            if (!authResult) return fail(new HttpError('Invalid current password', 404));

            // create new encrypted password
            const encryptedPassword = await bcrypt.hash(
                newPassword,
                isNaN(Number(process.env.ENCRYPT_SALT)) ? 10 : Number(process.env.ENCRYPT_SALT)
            );

            // update password
            await prisma.account.update({
                where: {
                    id: id
                },
                data: {
                    password: encryptedPassword
                }
            });

            return success(undefined);
        } else return fail(new HttpError('Account does not exist', 404));
    } catch (err) {
        return fail(new HttpError('Failed to update account password', 500));
    }
}

/**
 * Verify account and set last login time
 * @param email email of the account to verify
 * @param password plain password for verification
 * @returns id of verified account or undefined
 */
export async function verifyAccount (email: string, password: string): Promise<Result<string>> {
    try {
        const account = await prisma.account.findFirst({
            where: {
                email: email
            },
            select: {
                id: true,
                password: true
            }
        });

        if (account) {
            // compare password
            const authResult = await bcrypt.compare(password, account.password);
            if (!authResult) return fail(new HttpError('Authentication failed', 404));

            // set last login time
            await prisma.account.update({
                where: {
                    id: account.id
                },
                data: {
                    lastLogin: new Date()
                }
            });

            return success(account.id);
        } else return fail(new HttpError('Account does not exist', 404));
    } catch (err) {
        return fail(new HttpError('Failed to verify account', 500));
    }
}

/**
 * Check if account exists
 * @param accountId id of the account to check for
 * @returns boolean which represents true if account exists and false otherwise
 */
export async function accountExists (accountId: string): Promise<Result<boolean>> {
    try {
        const accountCount = await prisma.account.count({
            where: {
                id: accountId
            }
        });

        return success((accountCount > 0));
    } catch (err) {
        return fail(new HttpError('Failed to check for account', 500));
    }
}

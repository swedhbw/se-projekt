import express from 'express';
import 'dotenv/config';
import cors from 'cors';
import api from './api';
import { verifyJWT } from './middlewares/auth.middleware';
import { addTestData } from './config/mock-data.config';
import path from 'path';

const app = express();
const port = 3000;

app.use(cors());
app.use(express.json());

app.use('/api', verifyJWT, api);
app.use(express.static('src/dist/openthefridge'));
app.route('/*').get(function (req, res) {
    return res.sendFile(path.join(__dirname, 'dist/openthefridge/index.html'));
});

addTestData();

app.listen(port, () => {
    console.log(`Example app listening at http://localhost:${port}`);
});

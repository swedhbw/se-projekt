import express = require('express');
import { getAllIngredients } from '../providers/ingredient.provider';

const router = express.Router();

/**
 * Get all ingredients
 *
 * Returns:
 * status 200: array of ingredient names
 * status 400: if recipe id is not a number
 * status 500: if internal server error occurs
 */
router.get('', async (req, res) => {
    const result = await getAllIngredients();

    if (result.isSucc()) res.status(200).send(result.value);
    else res.status(result.value.httpStatus).send(result.value.message);
});

export = router;

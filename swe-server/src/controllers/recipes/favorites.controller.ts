import express from 'express';
import { getFavoriteRecipes } from '../../providers/recipe.provider';
import { RecipeRequest } from '../../types/ingredient-request.type';

const router = express.Router({ mergeParams: true });

/**
 * Get all favorite recipes of a user
 *
 * Params:
 * recipeId - the id of the recipe
 *
 * Returns:
 * status 200: the favorite recipes of the user
 * status 400: if the recipe id is not a number
 * status 500: if the database fails
 */
router.get('', async (req: RecipeRequest, res) => {
    const result = await getFavoriteRecipes(Number(req.params.recipeId));

    if (result.isSucc()) res.status(200).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

export = router;

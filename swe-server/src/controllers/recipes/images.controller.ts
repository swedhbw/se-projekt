import express from 'express';
import { getImagePath, ImageType } from '../../helpers/image.helper';
import { RecipeRequest } from '../../types/ingredient-request.type';

const router = express.Router({ mergeParams: true });

/**
 * Get one image by id
 *
 * Params:
 * recipeId - id of the recipe
 *
 * Returns:
 * status 200: Image
 * status 404: Image not found
 * status 500: Error in finding image
 */
router.get('', async (req: RecipeRequest, res) => {
    const id = req.params.recipeId;

    const result = await getImagePath(ImageType.RecipeShowcase, id);
    if (result.isSucc()) return res.status(200).sendFile(result.value);

    const defaultResult = await getImagePath(ImageType.RecipeShowcase, 'default');
    if (defaultResult.isSucc()) return res.status(200).sendFile(defaultResult.value);

    res.status(500).send({ message: 'Failed to get Image' });
});

export = router;

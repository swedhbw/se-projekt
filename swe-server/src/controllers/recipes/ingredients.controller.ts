import express from 'express';
import { getAllIngredientsOfRecipe, getIngredient } from '../../providers/ingredient.provider';
import { RecipeRequest } from '../../types/ingredient-request.type';

const router = express.Router({ mergeParams: true });

/**
 * Get all ingredients for a recipe
 *
 * Params:
 * recipeId - the id of the recipe
 *
 * Returns:
 * status 200: the ingredients for the recipe
 * status 400: if the recipeId is not a number
 * status 404: if the recipe does not exist
 * status 500: if the database fails
 */
router.get('', async (req: RecipeRequest, res) => {
    const { recipeId } = req.params;

    const result = await getAllIngredientsOfRecipe(Number(recipeId));

    if (result.isSucc()) return res.status(200).send(result.value);
    else { return res.status(result.value.httpStatus).send({ message: result.value.message }); }
});

/**
 * Get a specific ingredient for a recipe
 *
 * Params:
 * recipeId - the id of the recipe
 * name - the name of the ingredient
 *
 * Returns:
 * status 200: the ingredient
 * status 400: if the recipeId is not a number
 * status 404: if the recipe does not exist
 * status 500: if the database fails
 */
router.get('/:name', async (req: RecipeRequest, res) => {
    const { recipeId, name } = req.params;

    const result = await getIngredient(Number(recipeId), name);

    if (result.isSucc()) return res.status(200).send(result.value);
    else { return res.status(result.value.httpStatus).send({ message: result.value.message }); }
});

export = router;

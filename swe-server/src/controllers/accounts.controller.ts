import express from 'express';
import passwordRouter from './accounts/password.controller';
import favoritesRouter from './accounts/favorites.controller';
import recipeRouter from './accounts/recipes.controller';
import profileRouter from './accounts/profiles.controller';
import { areNotNullOrEmpty } from '../helpers/body-checker.helper';
import { authRestrict } from '../middlewares/auth.middleware';
import {
    createAccount,
    deleteAccount,
    getAccountById,
    updateAccount
} from '../providers/account.provider';

const router = express.Router();

router.use('/password', authRestrict, passwordRouter);
router.use('/favorites', authRestrict, favoritesRouter);
router.use('/recipes', authRestrict, recipeRouter);
router.use('/:accountId/profiles', profileRouter);

/**
 * Register new account
 *
 * Body:
 * name - of the client
 * email - of the client
 * password - of the client in plain format
 *
 * Returns:
 * status 201: Created account
 * status 400: Missing required fields
 * status 500: Failed to create account
 */
router.post('', async (req, res) => {
    const { name, email, password } = req.body;

    if (!areNotNullOrEmpty([name, email, password])) return res.status(400).send({ message: 'Body incorrect' });

    // TODO: check for valid email adress

    const result = await createAccount(name, email, password);

    if (result.isSucc()) res.status(201).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Get account by id
 *
 * Header:
 * token - token of session with account id
 *
 * Params:
 * accountId
 *
 * Returns:
 * status 200: Account model
 * status 401: Unauthorized
 * status 500: Failed to get account
 */
router.get('', authRestrict, async (req, res) => {
    const accountId = req.currAccount.accountId;

    const result = await getAccountById(accountId);

    if (result.isSucc()) res.status(200).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Update account information's
 *
 * Header:
 * token - token of session with account id
 *
 * Body:
 * email - of the client
 * name - of the client
 *
 * Returns:
 * status 200: New account model
 * status 400: Missing required fields
 * status 401: Unauthorized
 * status 404: Account not found
 *
 */
router.patch('', authRestrict, async (req, res) => {
    const accountId = req.currAccount.accountId;
    const { email, name } = req.body;

    // check if one of the fields is given
    if (!name && !email) return res.status(400).send({ message: 'missing required fields' });

    // get current account model
    const accountResult = await getAccountById(accountId);
    if (accountResult.isFail()) return res.status(404).send({ message: 'account does not exist' });

    // update account model with new values
    if (email) accountResult.value.email = email;
    if (name) accountResult.value.name = name;
    const result = await updateAccount(accountResult.value);

    if (result.isSucc()) res.status(200).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Delete account by it's id
 *
 * Header:
 * token - token of session with account id
 *
 * Returns:
 * status 200: Deleted account model
 * status 400: Missing required fields
 * status 401: Unauthorized
 * status 500: Failed to delete account
 */
router.delete('', authRestrict, async (req, res) => {
    const accountId = req.currAccount.accountId;

    if (!areNotNullOrEmpty([accountId])) return res.status(400).send({ message: 'Missing required fields' });

    const result = await deleteAccount(accountId);

    if (result.isSucc()) res.status(200).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

export = router;

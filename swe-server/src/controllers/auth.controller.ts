import express from 'express';
import 'dotenv';
import jwt from 'jsonwebtoken';
import { areNotNullOrEmpty } from '../helpers/body-checker.helper';
import { verifyAccount } from '../providers/account.provider';
import { authRestrict } from '../middlewares/auth.middleware';

const router = express.Router();

const TO_SECONDS = (60 * 60);
const DEFAULT_EXPIRATION = 5;
const expiration = (isNaN(Number(process.env.JWT_EXPIRATION_IN_HOURS))
    ? DEFAULT_EXPIRATION
    : Number(process.env.JWT_EXPIRATION_IN_HOURS)) * TO_SECONDS;

/**
 * Login route to authenticate client
 *
 * Body:
 * email
 * password
 *
 * Returns:
 * status 200: JWT token
 * status 400: Missing required fields
 * status 401: Invalid credentials
 * status 500: Failed to create token
 */
router.post('/login', async (req, res) => {
    const { email, password } = req.body;

    if (!areNotNullOrEmpty([email, password])) return res.status(400).send({ message: 'Body incorrect' });

    const result = await verifyAccount(email, password);

    if (result.isSucc()) {
        const token = createToken(result.value);

        res.status(201).send({ token: token, expiresIn: expiration, message: 'Login successful' });
    } else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Refresh authentication of client
 *
 * Header:
 * token - token of session with account id
 *
 * Returns:
 * status 200: JWT token
 * status 401: Unauthorized
 * status 500: Failed to create token
 */
router.get('/refresh', authRestrict, async (req, res) => {
    const token = createToken(req.currAccount.accountId);
    res.status(200).send({ token: token, expiresIn: expiration, message: 'Refresh successful' });
});

/**
 * Create token for specific client
 * @param accountId ID of the client
 * @returns JWT token as string
 */
function createToken (accountId: string): string {
    return jwt.sign({ accountId: accountId }, process.env.JWT_SECRET || 'secret', { algorithm: 'HS256', expiresIn: expiration });
}

export = router;

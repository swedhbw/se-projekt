import express from 'express';
import { getProfile } from '../../providers/account/profile.provider';
import { AccountRequest } from '../../types/account-request.type';

const router = express.Router({ mergeParams: true });

/**
 * Get profile of account
 *
 * params:
 * accountId: universally unique identifier of the account
 *
 * returns:
 * status 200: profile model of the account
 * status 404: account does not exist
 * status 500: failed to get profile
 */
router.get('', async (req: AccountRequest, res) => {
    const accountId = req.params.accountId;

    const result = await getProfile(accountId);

    if (result.isSucc()) res.status(200).send(result.value);
    else res.status(result.value.httpStatus).send(result.value.message);
});

export = router;

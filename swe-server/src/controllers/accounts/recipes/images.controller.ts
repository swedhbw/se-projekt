import express from 'express';
import fileUpload from 'express-fileupload';
import { checkForExistingImage, deleteImage, ImageType, saveImage } from '../../../helpers/image.helper';
import { RecipeRequest } from '../../../types/ingredient-request.type';
import imageController from '../../recipes/images.controller';

const router = express.Router({ mergeParams: true });

router.use(fileUpload({
    createParentPath: true, preserveExtension: true, safeFileNames: true
}));

// get one image by id
router.use('', imageController);

/**
 * Uploads an image for the recipe
 *
 * Header:
 * author - account id of the account extracted from token
 *
 * Params:
 * recipeId - id of the recipe
 *
 * Body:
 * 201: if the image was uploaded successfully
 * 400: if the image is not valid
 * 401: if the user is not authorized
 * 403: if the user is not the author of the recipe
 * 404: if the recipe does not exist
 * 500: if the image could not be uploaded
 */
router.post('', async (req: RecipeRequest, res) => {
    const recipeId = req.params.recipeId;

    const checkResult = await checkForExistingImage(ImageType.RecipeShowcase, recipeId);
    if (checkResult.isFail()) {
        return res.status(checkResult.value.httpStatus).send({ message: checkResult.value.message });
    } else if (checkResult.value) {
        return res.status(403).send({ message: 'Image already exists' });
    }

    if (req.files && req.files.recipePicture) {
        const picture: any = req.files.recipePicture;

        const result = await saveImage(picture, ImageType.RecipeShowcase, recipeId);
        if (result.isFail()) return res.status(result.value.httpStatus).send({ message: result.value.message });

        res.status(201).send({ message: 'Recipe picture uploaded' });
    } else res.status(400).send({ message: 'No image provided' });
});

/**
 * Patch one image by id
 *
 * Params:
 * imageID - id of the image
 *
 * Returns:
 * status 200: Image updated
 * status 400: No image provided
 * status 404: Image not found
 * status 500: Error in finding image
 */
router.put('', async (req: RecipeRequest, res) => {
    const recipeId = req.params.recipeId;

    if (req.files && req.files.recipePicture) {
        const file: any = req.files.recipePicture;

        const imageExists = await checkForExistingImage(ImageType.RecipeShowcase, recipeId);
        if (imageExists.isFail()) return res.status(imageExists.value.httpStatus).send({ message: imageExists.value.message });

        if (imageExists.isSucc() && imageExists.value) {
            const deleteResult = await deleteImage(ImageType.RecipeShowcase, recipeId);
            if (deleteResult.isFail()) return res.status(deleteResult.value.httpStatus).send({ message: deleteResult.value.message });
        }

        const result = await saveImage(file, ImageType.RecipeShowcase, recipeId);
        if (result.isFail()) return res.status(result.value.httpStatus).send({ message: result.value.message });

        res.status(200).send({ message: 'Recipe picture updated' });
    } else {
        res.status(400).send({ message: 'No image provided' });
    }
});

/**
 * Delete image by id
 *
 * Params:
 * imageID - id of the image
 *
 * Returns:
 * status 204: Image deleted
 * status 404: Image not found
 * status 500: Error in finding image
 */
router.delete('', async (req: RecipeRequest, res) => {
    const recipeId = req.params.recipeId;

    const result = await deleteImage(ImageType.RecipeShowcase, recipeId);

    if (result.isSucc()) {
        res.status(204).send({ message: 'Image deleted' });
    } else res.status(result.value.httpStatus).send({ message: result.value.message });
});

export = router;

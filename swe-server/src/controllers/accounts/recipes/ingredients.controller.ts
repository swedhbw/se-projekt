import express from 'express';
import { areNotNullOrEmpty, areNumbers } from '../../../helpers/body-checker.helper';
import { addIngredients, removeAllIngredients, removeIngredient } from '../../../providers/ingredient.provider';
import { RecipeRequest } from '../../../types/ingredient-request.type';

const router = express.Router({ mergeParams: true });

/**
 * Add an ingredient to a recipe
 *
 * Header:
 * author - account id of the account extracted from token
 *
 * Body:
 * Array of ingredients to add with (name, amount and unit)
 *
 * Returns:
 * status 201: the new ingredients
 * status 400: if the body is not valid
 * status 401: if the account is not authorized
 * status 403: if the recipe is not owned by the account
 * status 404: if the recipe does not exist
 * status 500: if the database fails
 */
router.post('', async (req: RecipeRequest, res) => {
    const { recipeId } = req.params;
    const bodyIngredients = req.body;

    // check if body is an array
    if (!Array.isArray(bodyIngredients)) {
        return res.status(400).send({
            message: 'Request body must be an array of ingredients'
        });
    }

    const ingredients = new Array<{name: string, amount: number, unit: number}>();

    // check if ingredients are valid
    for (const { name, amount, unit } of bodyIngredients) {
        if (!areNotNullOrEmpty([name]) || !areNumbers([amount, unit])) {
            return res.status(400).send({
                message: 'Ingredients must have a name, amount and unit'
            });
        } else ingredients.push({ name, amount, unit });
    }

    const result = await addIngredients(Number(recipeId), ingredients);

    if (result.isSucc()) return res.status(201).send(result.value);
    else { return res.status(result.value.httpStatus).send({ message: result.value.message }); }
});

/**
 * Remove an ingredient from a recipe
 *
 * Header:
 * author - account id of the account extracted from token
 *
 * Params:
 * recipeId - the id of the recipe
 * name - the name of the ingredient
 *
 * Returns:
 * status 200: the removed ingredient
 * status 400: if the recipeId is not a number
 * status 401: if the account is not authorized
 * status 403: if the recipe is not owned by the account
 * status 404: if the recipe does not exist
 * status 500: if the database fails
 */
router.delete('/:name', async (req: RecipeRequest, res) => {
    const { recipeId, name } = req.params;

    const result = await removeIngredient(Number(recipeId), name);

    if (result.isSucc()) return res.status(200).send(result.value);
    else { return res.status(result.value.httpStatus).send({ message: result.value.message }); }
});

/**
 * Remove all ingredients from a recipe
 *
 * Header:
 * author - account id of the account extracted from token
 *
 * Params:
 * recipeId - the id of the recipe
 *
 * Returns:
 * status 200: if removed successfully
 * status 400: if the recipeId is not a number
 * status 401: if the account is not authorized
 * status 403: if the recipe is not owned by the account
 * status 500: if the database fails
 */
router.delete('', async (req: RecipeRequest, res) => {
    const { recipeId } = req.params;

    const result = await removeAllIngredients(Number(recipeId));

    if (result.isSucc()) return res.status(200).send();
    else { return res.status(result.value.httpStatus).send({ message: result.value.message }); }
});

export = router;

import express from 'express';
import ingredientRouter from './recipes/ingredients.controller';
import imageController from './recipes/images.controller';
import { areNotNullOrEmpty, areNumbers } from '../../helpers/body-checker.helper';
import { createRecipe, deleteRecipe, getRecipesByAuthor, patchRecipe } from '../../providers/recipe.provider';
import { checkForAuthorMiddle, checkRecipeIdMiddle } from '../../middlewares/recipe.middleware';
import { deleteImage, ImageType } from '../../helpers/image.helper';

const router = express.Router();

router.use('/:recipeId/ingredients', checkRecipeIdMiddle, checkForAuthorMiddle, ingredientRouter);
router.use('/:recipeId/images', checkRecipeIdMiddle, checkForAuthorMiddle, imageController);

/**
 * Create new recipe
 *
 * Header:
 * author - account id of the account extracted from token
 *
 * Body:
 * name - name of the recipe
 * instructions - instructions for the recipe
 * isPublic - is the recipe public
 * picture - picture (optional: if not provided, the recipe will have a default picture)
 * totalTime - total time of the recipe
 * prepTime - preparation time of the recipe
 * difficulty - difficulty of the recipe
 *
 * Returns:
 * status 201: Created recipe
 * status 400: Missing required fields
 * status 401: Unauthorized
 * status 500: Failed to create recipe
 */
router.post('', async (req, res) => {
    const { name, instructions, isPublic, totalTime, prepTime, difficulty, portions } = req.body;
    const author = req.currAccount.accountId;

    if (!areNotNullOrEmpty([name, instructions, difficulty, isPublic, portions])) return res.status(400).send({ message: 'Body incorrect' });
    if (!areNumbers([totalTime, prepTime, difficulty, portions])) return res.status(400).send({ message: 'Parameters incorrect: total time, difficulty, portions and preparing time must be numeric' });

    const result = await createRecipe(name, instructions, author, Boolean(isPublic), Number(totalTime), Number(prepTime), Number(difficulty), Number(portions));

    if (result.isSucc()) res.status(201).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Get all recipes by author
 *
 * Header:
 * author - account id of the account extracted from token
 *
 * Returns:
 * status 200: Array of recipe models
 * status 401: Unauthorized
 * status 500: Failed to get recipes
 */
router.get('', async (req, res) => {
    const author = req.currAccount.accountId;

    const result = await getRecipesByAuthor(author);
    if (result.isSucc()) {
        if (req.currAccount) {
            for (const recipe of result.value) await recipe.setFavorite(req.currAccount.accountId);
        }

        res.status(200).send(result.value);
    } else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Update recipe by id
 *
 * Header:
 * author - account id of the account extracted from token
 *
 * Params:
 * recipeId - id of the recipe
 *
 * Body:
 * name - name of the recipe
 * instructions - instructions for the recipe
 * isPublic - is the recipe public
 * picture - picture link of the recipe
 * totalTime - total time of the recipe
 * prepTime - preparation time of the recipe
 * difficulty - difficulty of the recipe
 *
 * Returns:
 * status 200: Updated recipe
 * status 400: Missing required fields
 * status 401: Unauthorized
 * status 403: user is not the author
 * status 404: Recipe not found
 * status 500: Failed to update recipe
 */
router.patch('/:recipeId', checkRecipeIdMiddle, checkForAuthorMiddle, async (req, res) => {
    const { name, instructions, isPublic, totalTime, prepTime, difficulty, portions } = req.body;
    const { recipeId } = req.params;

    if (!areNotNullOrEmpty([name, instructions, isPublic, difficulty])) return res.status(400).send({ message: 'Missing fields in Body' });
    if (!areNumbers([recipeId, totalTime, prepTime, portions])) return res.status(400).send({ message: 'Parameters incorrect: recipe id, total time, portions and preparing time must be numeric' });

    const result = await patchRecipe(Number(recipeId), name, instructions, isPublic, totalTime, prepTime, difficulty, portions);

    if (result.isSucc()) res.status(200).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Delete recipe by id
 *
 * Header:
 * author - account id of the account extracted from token
 *
 * Params:
 * recipeId - id of the recipe
 *
 * Returns:
 * status 200: Deleted recipe
 * status 401: Unauthorized
 * status 403: user is not the author
 * status 404: Recipe not found
 * status 500: Failed to delete recipe
 */
router.delete('/:recipeId', checkRecipeIdMiddle, checkForAuthorMiddle, async (req, res) => {
    const { recipeId } = req.params;

    const result = await deleteRecipe(Number(recipeId));
    await deleteImage(ImageType.RecipeShowcase, recipeId);

    if (result.isSucc()) res.status(200).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

export = router;

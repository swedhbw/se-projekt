// Account password router
import { Router } from 'express';
import { areNotNullOrEmpty } from '../../helpers/body-checker.helper';
import { updateAccountPassword } from '../../providers/account.provider';

const router = Router();

/**
 * Update password of account
 *
 * Header:
 * accountId - account id of the account extracted from token
 *
 * Body:
 * currPassword: current password of the user
 * newPassword: new password of the user
 *
 * Returns:
 * status 200: Password successfully updated
 * status 400: Missing required fields
 * status 404: Account does not exist
 * status 401: Invalid current password
 * status 500: Failed to update account password
 */
router.put('', async (req, res) => {
    const accountId = req.currAccount.accountId;
    const { currPassword, newPassword } = req.body;

    if (!areNotNullOrEmpty([currPassword, newPassword])) {
        return res.status(400).send({
            message: 'Missing required fields'
        });
    }

    const passwordChangeResult = await updateAccountPassword(accountId, currPassword, newPassword);

    if (passwordChangeResult.isSucc()) {
        return res.status(201).send({
            message: 'Password successfully updated'
        });
    } else res.status(passwordChangeResult.value.httpStatus).send({ message: passwordChangeResult.value.message });
});

export = router;

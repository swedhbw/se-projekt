import express from 'express';
import { areNumbers } from '../../helpers/body-checker.helper';
import { checkRecipeIdMiddle } from '../../middlewares/recipe.middleware';
import { createFavorite, deleteFavorite, getFavoritesByAccount } from '../../providers/favorite.provider';

const router = express.Router({ mergeParams: true });

/**
 * Create new favorite
 *
 * Header:
 * accountId - account id of the account extracted from token
 *
 * Body:
 * recipeId - of the recipe
 *
 * Returns:
 * status 201: New favorite model
 * status 400: Missing required fields
 * status 401: Unauthorized
 * status 500: Failed to create favorite
 */
router.post('', async (req, res) => {
    const { recipeId } = req.body;
    const accountId = req.currAccount.accountId;

    if (!areNumbers([recipeId])) return res.status(400).send({ message: 'Body incorrect: provide "recipe" from recipe' });

    const result = await createFavorite(accountId, Number(recipeId));

    if (result.isSucc()) res.status(201).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Get all favorites by account
 *
 * Header:
 * accountId - account id of the account extracted from token
 *
 * Returns:
 * status 200: Array of favorite models
 * status 401: Unauthorized
 * status 500: Failed to get favorites
 */
router.get('', async (req, res) => {
    const accountId = req.currAccount.accountId;

    const result = await getFavoritesByAccount(accountId);

    if (result.isSucc()) res.status(200).send(result.value);
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Delete favorite by account and recipe id
 *
 * Header:
 * accountId - account id of the account extracted from token
 *
 * Params:
 * accountId - id of the account
 * recipeId - id of the recipe
 *
 * Returns:
 * status 204: Deleted favorite model
 * status 401: Unauthorized
 * status 500: Failed to delete favorite
 */
router.delete('/:recipeId', checkRecipeIdMiddle, async (req, res) => {
    const { recipeId } = req.params;
    const accountId = req.currAccount.accountId;

    if (!areNumbers([recipeId])) return res.status(400).send({ message: 'Parameters incorrect: recipeId must be numeric' });

    const result = await deleteFavorite(accountId, Number(recipeId));

    if (result.isSucc()) res.status(204).send();
    else res.status(result.value.httpStatus).send({ message: result.value.message });
});

export = router;

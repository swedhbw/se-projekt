import express from 'express';
import ingredientRouter from './recipes/ingredients.controller';
import favoritesRouter from './recipes/favorites.controller';
import imageController from './recipes/images.controller';
import { getRecipeById, queryRecipes } from '../providers/recipe.provider';
import { checkRecipeIdMiddle } from '../middlewares/recipe.middleware';
import { areNotNullOrEmpty } from '../helpers/body-checker.helper';

const router = express.Router();

router.use('/:recipeId/favorites', checkRecipeIdMiddle, favoritesRouter);
router.use('/:recipeId/ingredients', checkRecipeIdMiddle, ingredientRouter);
router.use('/:recipeId/images', checkRecipeIdMiddle, imageController);

/**
 * Get a recipe by its id
 *
 * Params:
 * recipeId - the id of the recipe
 *
 * Returns:
 * status 200: the recipe
 * status 400: if the recipe id is not a number
 * status 404: if the recipe id is not found
 * status 500: if the database fails
 */
router.get('/:recipeId', checkRecipeIdMiddle, async (req, res) => {
    const result = await getRecipeById(Number(req.params.recipeId));

    if (result.isSucc()) {
        if (req.currAccount) await result.value.setFavorite(req.currAccount.accountId);

        res.status(200).send(result.value);
    } else res.status(result.value.httpStatus).send({ message: result.value.message });
});

/**
 * Get recipes by query
 *
 * Params:
 * ingredients: array of ingredients - the query to search for
 * fromAccountId: the id of the account from which the recipes are being queried
 *
 * Returns:
 * status 200: array of recipes
 * status 400: if body is not valid
 * status 500: if internal server error occurs
 */
router.patch('', async (req, res) => {
    const { ingredients, fromAccountId, portions } = req.body;
    if (!Array.isArray(ingredients) || !areNotNullOrEmpty([portions]) || portions === 0) return res.status(400).send({ message: 'Body incorrect' });
    let loggedInAccountId = null;
    if (req.currAccount) {
        loggedInAccountId = req.currAccount.accountId;
    }
    const result = await queryRecipes(ingredients, fromAccountId, portions, loggedInAccountId);

    if (result.isSucc()) {
        if (req.currAccount) {
            for (const recipeArray of result.value.values) {
                for (const recipe of recipeArray) {
                    await recipe.setFavorite(req.currAccount.accountId);
                }
            }
        }

        const object: any = {};

        for (let i = 0; i < result.value.length; i++) {
            object[result.value.keys[i]] = result.value.values[i];
        }

        res.status(200).send(object);
    } else res.status(result.value.httpStatus).send({ message: result.value.message });
});

export = router;

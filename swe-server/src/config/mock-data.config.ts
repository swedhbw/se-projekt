import 'dotenv';
import { Difficulty } from '../models/difficulty.model';
import { Recipe } from '../models/recipe.model';
import { createAccount, getAccountByEmail } from '../providers/account.provider';
import { createFavorite } from '../providers/favorite.provider';
import { addIngredients } from '../providers/ingredient.provider';
import { createRecipe } from '../providers/recipe.provider';

// Add test data to prisma database
export async function addTestData () {
    const needMock = process.env.MOCK_DATA;

    if (needMock !== 'true') return;
    const account = await getAccountByEmail('mock@mail.com');
    if (account.isSucc()) return;

    await createAccount('Mock', 'mock@mail.com', 'mock');

    const firstAccount = await createAccount('Anna', 'anna@mail.com', '123456');
    const firstAccountRecipes: Array<Recipe> = [];
    if (firstAccount.isSucc()) {
        const annaRecipe = await createRecipe(
            'Strawberry Mango Dream (4 servings)',
            'Purée the strawberries and mangoes. Then add the sugar and the grated lemon peel. Mix everything well again and refrigerate.\n Later, serve on natural yoghurt (also works with curd cheese), preferably in a glass.\nFinally, decorate with a few mint leaves and the perfect summer dessert is ready!',
            firstAccount.value.id,
            true,
            10,
            10,
            Difficulty.MEDIUM,
            4
        );
        if (annaRecipe.isSucc()) {
            const ingredients = Array<{name: string, amount: number, unit: number}>();

            ingredients.push({ name: 'Strawberries', amount: 200, unit: 1 });
            ingredients.push({ name: 'Mango', amount: 150, unit: 1 });
            ingredients.push({ name: 'Sugar', amount: 15, unit: 1 });
            ingredients.push({ name: 'Mint leaves', amount: 1, unit: 2 });
            ingredients.push({ name: 'Natural yoghurt', amount: 15, unit: 1 });

            await addIngredients(annaRecipe.value.id, ingredients);

            firstAccountRecipes.push(annaRecipe.value);
        }

        const annaRecipe2 = await createRecipe(
            'Pancakes',
            'Mix the milk and natural yoghurt. I use about 200 ml yoghurt, the rest milk. You can also use more or less yoghurt. This makes the pancakes really thick and moist!\nGradually add the flour, baking powder, milk and yoghurt mixture, salt and sugar to the eggs and keep stirring. Add a little more milk if the batter is too thick. But it should already be thick enough.\nHeat the pan over medium heat, pour in about a tsp of oil at a time and fry the whole batter into pancakes, one after the other. Important! Use very little oil and fry slowly, i.e. on medium heat, not too hot.\n',
            firstAccount.value.id,
            false,
            20,
            10,
            Difficulty.EASY,
            3
        );
        if (annaRecipe2.isSucc()) {
            const ingredients = Array<{name: string, amount: number, unit: number}>();

            ingredients.push({ name: 'Egg', amount: 3, unit: 2 });
            ingredients.push({ name: 'Milk', amount: 100, unit: 1 });
            ingredients.push({ name: 'Natural yoghurt', amount: 200, unit: 1 });
            ingredients.push({ name: 'Baking powder', amount: 1, unit: 2 });
            ingredients.push({ name: 'Sugar', amount: 2, unit: 3 });

            await addIngredients(annaRecipe2.value.id, ingredients);

            firstAccountRecipes.push(annaRecipe2.value);
        }
    }

    const secondAccount = await createAccount('Bob', 'bob@mail.com', '654321');
    if (secondAccount.isSucc()) {
        const bobRecipe = await createRecipe(
            'Creamy Spinach and Artichoke Dip',
            'In a large bowl, combine the spinach, artichoke hearts, garlic, and olive oil. Toss to combine. Season with salt and pepper.\nIn a small bowl, combine the ricotta, parmesan, and lemon juice.\nIn a large skillet, heat the olive oil over medium heat. Add the spinach and artichoke hearts and cook until the spinach is tender, about 5 minutes.\nAdd the garlic and cook until fragrant, about 1 minute.\nAdd the ricotta and cook until heated through, about 1 minute.\nAdd the lemon juice and cook until the cream is thickened, about 1 minute.\nServe immediately with bread crumbs and parmesan cheese.\n',
            secondAccount.value.id,
            true,
            10,
            10,
            Difficulty.MEDIUM,
            2
        );
        if (bobRecipe.isSucc()) {
            const ingredients = Array<{name: string, amount: number, unit: number}>();

            ingredients.push({ name: 'Spinach', amount: 200, unit: 1 });
            ingredients.push({ name: 'Artichoke hearts', amount: 150, unit: 1 });
            ingredients.push({ name: 'Garlic', amount: 1, unit: 2 });
            ingredients.push({ name: 'Olive oil', amount: 15, unit: 1 });
            ingredients.push({ name: 'Salt', amount: 1, unit: 3 });
            ingredients.push({ name: 'Pepper', amount: 1, unit: 2 });
            ingredients.push({ name: 'Ricotta', amount: 200, unit: 1 });
            ingredients.push({ name: 'Parmesan', amount: 200, unit: 1 });
            ingredients.push({ name: 'Lemon juice', amount: 15, unit: 1 });

            await addIngredients(bobRecipe.value.id, ingredients);
        }

        if (firstAccountRecipes.length > 0) createFavorite(secondAccount.value.id, firstAccountRecipes[0].id);
    }
}

import { Response, NextFunction } from 'express';
import { areNumbers } from '../helpers/body-checker.helper';
import { checkForAuthor } from '../providers/recipe.provider';
import { RecipeRequest } from '../types/ingredient-request.type';

// Check if the recipe id is a number
export const checkRecipeIdMiddle = async (req: RecipeRequest, res: Response, next: NextFunction) => {
    const { recipeId } = req.params;
    if (!areNumbers([recipeId])) {
        return res.status(400).send({
            message: 'Recipe id must be a number'
        });
    }

    next();
};

// Check if the user is the author of the recipe
export const checkForAuthorMiddle = async (req: RecipeRequest, res: Response, next: NextFunction) => {
    const { recipeId } = req.params;
    const accountId = req.currAccount.accountId;

    const result = await checkForAuthor(Number(recipeId), accountId);

    if (result.isSucc()) {
        if (result.value) next();
        else {
            res.status(403).send({
                message: 'You are not the author of this recipe'
            });
        }
    } else res.status(result.value.httpStatus).send(result.value.message);
};

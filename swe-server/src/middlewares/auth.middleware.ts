import { NextFunction, Request, Response } from 'express';
import jwt, { JsonWebTokenError } from 'jsonwebtoken';
import { RequestAccount } from '../models/request-account.model';
import { accountExists } from '../providers/account.provider';

interface DecodedJWT {
    accountId: string;
    iat: number;
    exp: number;
}

/**
 * check for existing JWT token and integrated account id and pass it to the next route
 */
export const verifyJWT = async (req: Request, res: Response, next: NextFunction) => {
    // check for existing JWT
    if (req.headers && req.headers.authorization && req.headers.authorization.split(' ')[0] === 'Bearer') {
        try {
            const account = await verifyToken(req.headers.authorization);
            req.currAccount = account;
        } catch (err) {}
    }
    next();
};

export const authRestrict = async (req: Request, res: Response, next: NextFunction) => {
    // check for existing account
    if (req.currAccount) next();
    else res.status(401).send('Unauthorized');
};

async function verifyToken (token: string): Promise<RequestAccount> {
    // verify JWT token
    const decoded = jwt.verify(token.split(' ')[1], process.env.JWT_SECRET || 'secret') as DecodedJWT;
    // check for account id
    if (!decoded.accountId) throw new JsonWebTokenError('ID of account not given');

    // check if account exists
    const accountResult = await accountExists(decoded.accountId);

    if (accountResult.isSucc() && accountResult.value) {
        // add account id to the request
        return new RequestAccount(decoded.accountId);
    } else throw new JsonWebTokenError('Account does not exist');
}

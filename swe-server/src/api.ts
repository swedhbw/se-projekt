import express from 'express';
import recipeRouter from './controllers/recipes.controller';
import ingredientRouter from './controllers/ingredients.controller';
import accountRouter from './controllers/accounts.controller';
import authRouter from './controllers/auth.controller';

const router = express.Router();

// defined routes
router.use('/recipes', recipeRouter);
router.use('/ingredients', ingredientRouter);
router.use('/accounts', accountRouter);
router.use('/auth', authRouter);

export = router;

import { RequestAccount } from '../../src/models/request-account.model';

// add account information's to global express request type definition
declare global {
    namespace Express {
        interface Request {
            currAccount: RequestAccount
        }
    }
}

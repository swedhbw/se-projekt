# Backend of the swe project

# How to start
You have two options to start the project. Both options depends on [docker].  
**If you want to start the project with hosting the frontend, you must compile the files to the `/swe-server/src/dist` folder.**

1. Start the hole project by typing **"docker-compose up"**. It creates the database, adminer (SQL management tool) and the swe-server.
2. To Start only the database and adminer you can type `docker-compose -f .\docker-compose-dev.yml up`. This docker-compose file is especially for development purposes.

If new changes were made, you must add **"--build"** to the end of the command you're using (`docker-compose up --build`). The command addition make's a new build of the current project.
# Tech and Dev

##  Folder structure
* `config/` – configuration of the app
* `controllers/` – defines your app routes and their logic
* `dist/` - this folder stores the build files of the frontend, so the server can provide it.
* `helpers/` – code and functionality to be shared by different parts of the project
* `middlewares/` – Express middlewares which process the incoming requests before handling them down to the routes
* `models/` – represents data, implements business logic
* `providers/` – handles storage
* `app.ts` – initializes the app and glues everything together
* `api.ts` – initializes all routes with their endpoint's'
* `package.json` – remembers all packages that your app depends on and their versions

Ref: [Express - Folder structure best practices](https://www.terlici.com/2014/08/25/best-practices-express-structure.html "Express - Folder structure best practices")

## Prisma
[Prisma] is an database toolkit for PostgreSQL, MySQL, SQL Server, SQLite and MongoDB.
* Database currently in use: **MariaDB/MySQL**

### Files and folders:
* **prisma/** – represents the folder for all prisma specific stuff
* **prisma/schema.prisma** – contains the defined database structure
* **prisma/migrations** – here you can find all the changes that were made to the database during development **(don't gitignore or delete the files)**

### Commands:
* **"npx prisma"** – base command for all the prisma commands
* **"npx prisma migrate dev --name [migration message]"** – [migrate] new changes of schema file to the database
* **"npx prisma migrate dev"** – [migrate] current defined structure to the database
* **"npx prisma generate"** – [generates prisma client] to use defined structure in code

## Environment files
The Environment file **".env"** contains all customizable variables to run the app properly (must be manually created).

Environment variables:
* **DATABASE_URL** – defines the [connection string] for the database: "DATABASE_URL=mysql://USER:PASSWORD@HOST:PORT/DATABASE".
* **ENCRYPT_SALT** - Salt for the password (ENCRYPT_SALT=10) encryption.
* **MOCK_DATA** - If the project should be initialized with some basic data (MOCK_DATA=true).

[Prisma]: <https://www.prisma.io/>
[connection string]: <https://www.prisma.io/docs/getting-started/setup-prisma/start-from-scratch/relational-databases/connect-your-database-typescript-postgres>
[migrate]: <https://www.prisma.io/docs/concepts/components/prisma-migrate>
[generates prisma client]: <https://www.prisma.io/docs/concepts/components/prisma-client>
[docker]: <https://www.docker.com/products/docker-desktop/>
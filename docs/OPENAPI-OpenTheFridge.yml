openapi: 3.0.0
info:
  title: "OpenTheFridge"
  description: "API zum Schreiben und Lesen aus einer Datenbank"
  version: 1.0.0
tags:
  - name: auth
    description: Authentifizierungs und Autorisierungs Routen
  - name: account
    description: Ein Account bei OpenTheFridge
  - name: profile
    description: Profile with public information of an account
  - name: recipe
    description: Ein Rezept mit Zutaten
  - name: favorite
    description: Ein favorisiertes Rezept eines Accounts
  - name: ingredient
    description: Eine Zutat in einem Rezept
  - name: image
    description: images

paths:
  /auth/login:
    post:
      tags:
        - auth
      summary: Login Route für einen Benutzer
      description: Login daten werden als Anfrage und ein JWT Token als Antwort gesendet
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                  description: Email des Benutzers (unique)
                  example: "max.mustermann@web.de"
                password:
                  type: string
                  description: Passwort des Benutzer Accounts
                  example: "Passwort123"
      responses:
        "200":
          description: erfolgreich eingeloggt
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/token"
        "400":
          description: falsch aufgebaute Anfrage
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/error"
        "403":
          description: Autorisierung ist fehlgeschlagen
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/error"
  /auth/refresh:
    get:
      tags:
        - auth
      summary: Erneuern eines Accesstokens
      description: Ein Token soll erneuert werden.
      security:
        - bearerAuth: []
      responses:
        "200":
          description: erfolgreich erneuert
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/token"
        "400":
          description: falsch aufgebaute Anfrage
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/error"
        "403":
          description: Autorisierung ist fehlgeschlagen
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/error"
  /recipes/{{recipeId}}/images:
    get:
      tags:
        - image
      summary: get image
      description: get image
      parameters:
        - in: path
          name: recipeId
          required: true
          description: ID of an recipe
          schema:
            type: number
      responses:
        "200":
          description: image
        "404":
          description: recipe does not exist
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: Internal server error
          content:
            aplication/json:
              schema:
                $ref: "#/components/schemas/error"
  /accounts/recipes/{{recipeId}}/images:
    post:
      tags:
        - image
      summary: Add image to an recipe
      description: Upload an image to the server to a specific recipe
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general information's and account id
          schema:
            $ref: "#/components/schemas/bearer"
        - in: path
          name: recipeId
          required: true
          description: ID of an recipe
          schema:
            type: integer
            format: int256
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                recipePicture:
                  type: string
                  format: binary
      responses:
        "200":
          description: Upload of image successfully
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ingredient"
        "401":
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "403":
          description: Not the author of the recipe
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    put:
      tags:
        - image
      summary: Replace an image of an recipe
      description: Upload an image to the server to a specific recipe and replace it with an existing one
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general information's and account id
          schema:
            $ref: "#/components/schemas/bearer"
        - in: path
          name: recipeId
          required: true
          description: ID of an recipe
          schema:
            type: integer
            format: int256
      requestBody:
        content:
          multipart/form-data:
            schema:
              type: object
              properties:
                recipePicture:
                  type: string
                  format: binary
      responses:
        "200":
          description: Replaced of image successfully
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ingredient"
        "401":
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "403":
          description: Not the author of the recipe
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    delete:
      tags:
        - image
      summary: Delete an image of an recipe
      description: Remove an image of the server of an specific recipe
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general information's and account id
          schema:
            $ref: "#/components/schemas/bearer"
        - in: path
          name: recipeId
          required: true
          description: ID of an recipe
          schema:
            type: integer
            format: int256
      responses:
        "200":
          description: Removed image successfully
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ingredient"
        "401":
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "403":
          description: Not the author of the recipe
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"

  /accounts:
    post:
      tags:
        - account
      summary: erstellt einen neuen Account
      description: erstellt einen neuen Account über eine mitgegebene Formdata
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              $ref: "#/components/schemas/account"
        description: der zu erstellende Account
      responses:
        "201":
          description: erfolgreich erstellt
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/accountWithoutPassword"
        "400":
          description: falsch aufgebaute Anfrage
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    get:
      tags:
        - account
      summary: Holt den Nutzer
      description: Der Nutzer wird auf basis des user id parameters innerhalb des Tokens gefunden und zurückgesandt
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
      responses:
        "200":
          description: erfolgreich erhalten
          content:
            application/json:
              schema:
                oneOf:
                  - $ref: "#/components/schemas/accountWithoutPassword"
                  - $ref: "#/components/schemas/profile"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    patch:
      tags:
        - account
      summary: Editiert den Nutzer
      description: Der Nutzer wird editiert. Die id befindet sich innehalb des Tokens
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              $ref: "#/components/schemas/account"
        description: der zu editierende Account
      responses:
        "200":
          description: erfolgreich verändert
          content:
            text/plain:
              schema:
                $ref: "#/components/schemas/account"
        "400":
          description: falsch aufgebaute Anfrage
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "401":
          description: nicht authentifiziert
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    delete:
      tags:
        - account
      summary: Löscht den Nutzer
      description: Der Nutzer wird gelöscht. Die ID befindet sich innerhalb des Tokens
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
      responses:
        "204":
          description: erfolgreich gelöscht
        "400":
          description: falsch aufgebaute Anfrage
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "401":
          description: nicht authentifiziert
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"

  /accounts/{accountId}/profiles:
    get:
      tags:
        - profile
      summary: Gets the profile of an account
      description: Gets public informations of an account and returns it as an profile model
      parameters:
        - in: path
          name: accountId
          required: true
          description: id of the account
          schema:
            type: integer
      responses:
        "200":
          description: successfull operation with profile
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/profile"
        "404":
          description: acccount doesn't exist
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        
  /accounts/favorites:
    get:
      tags:
        - favorite
      summary: Holt alle Favoriten eines Accounts
      description: Alle Favoriten werden gefunden
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
      responses:
        "200":
          description: erfolgreich erhalten
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/favorite"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    post:
      tags:
        - favorite
      summary: Erstellt ein Favorit eines Accounts über ein Rezept
      description: Ein Favorit wird erstellt
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
      security:
        - bearerAuth: []
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                recipeId:
                  type: integer
                  format: int256

        description: das zu favorisierende Rezept
      responses:
        "201":
          description: erfolgreich erstellt
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/favorite"
        "401":
          description: nicht authentifiziert
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
  /accounts/favorites/{recipeId}:
    delete:
      tags:
        - favorite
      summary: Löscht das favorisierte Rezept eines Nutzers
      description: Das favorisierte Rezept wird gelöscht
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
        - in: path
          name: recipeId
          required: true
          description: eine Id eines Rezeptes
          schema:
            type: integer
            format: int256
      responses:
        "204":
          description: erfolgreich gelöscht
        "401":
          description: nicht authentifiziert
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
  /recipes/{recipeId}/favorites:
    get:
      tags:
        - favorite
      summary: Holt alle Favoriten eines Rezeptes
      description: Alle Favoriten werden gefunden
      parameters:
        - in: path
          name: recipeId
          required: true
          description: eine Id
          schema:
            type: integer
            format: int256
      responses:
        "200":
          description: erfolgreich erhalten
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/favorite"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
  /recipes:
    patch:
      tags:
        - recipe
      summary: Holt alle Rezepte anhand der Kriterien im Body
      description: Alle treffenden Rezepte werden zurückgegeben
      responses:
        "200":
          description: erfolgreich erhalten
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/recipe"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              type: object
              properties:
                ingredients:
                  type: array
                  items:
                    $ref: "#/components/schemas/ingredient"
                  description: die zu suchenden Zutaten 
                fromAccountId:
                  type: string
                  description: Id des Accounts, von dem die Rezepte gesucht werden sollen (optional)
        description: das zu speichernde Rezept
  /recipes/{recipeId}:
    get:
      tags:
        - recipe
      summary: Holt ein Rezept aus einer Id
      description: Ein Rezept einer Id wird gefunden, da die Id eindeutig ist
      parameters:
        - in: path
          name: recipeId
          required: true
          description: Id of recipe
          schema:
            type: integer
            format: int256
      responses:
        "200":
          description: erfolgreich erhalten
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/recipe"
        "401":
          description: nicht authentifiziert
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
  /accounts/recipes:
    post:
      tags:
        - recipe
      summary: erstellt ein neues Rezept
      description: erstellt ein neues Rezept und speichert dies in die Datenbank
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              $ref: "#/components/schemas/recipeCreate"
        description: das zu speichernde Rezept
      responses:
        "201":
          description: erfolgreich angelegt
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/recipe"
        "400":
          description: falsch aufgebaute Anfrage
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "401":
          description: nicht authentifiziert
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    get:
      tags:
        - recipe
      summary: Holt alle Rezepte eines Accounts
      description: Alle Rezepte eines Accounts werden gefunden
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
      responses:
        "200":
          description: erfolgreich erhalten
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/recipe"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
  /accounts/recipes/{recipeId}:
    patch:
      tags:
        - recipe
      summary: Editiert das Rezept einer Id
      description: Das Rezept mit der spezifizierten Id wird editiert, da die Id einmalig in den Rezepten in der Datenbank auftreten kann
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
        - in: path
          name: recipeId
          required: true
          description: id of the recipe
          schema:
            type: integer
            format: int256
      requestBody:
        content:
          application/x-www-form-urlencoded:
            schema:
              $ref: "#/components/schemas/recipe"
        description: das zu speichernde Rezept
      responses:
        "200":
          description: erfolgreich verändert
          content:
            text/plain:
              schema:
                $ref: "#/components/schemas/recipe"
        "400":
          description: falsch aufgebaute Anfrage
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "401":
          description: nicht authentifiziert
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"

        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    delete:
      tags:
        - recipe
      summary: Löscht das Rezept einer Id
      description: Das Rezept wird mit der eindeutigen Id gelöscht
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
        - in: path
          name: recipeId
          required: true
          description: ID of the recipe
          schema:
            type: integer
            format: int256
      responses:
        "204":
          description: erfolgreich gelöscht
        "400":
          description: falsch aufgebaute Anfrage
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "401":
          description: nicht authentifiziert
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
  /ingredients:
    get:
      tags:
        - ingredient
      summary: Holt alle Zutaten
      description: Es werden alle (einzigartigen) Zutaten erhalten.
      responses:
        "200":
          description: erfolgreich erhalten
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ingredient"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
  /recipes/{recipeId}/ingredients:
    get:
      tags:
        - ingredient
      summary: Holt alle Zutaten eines Rezeptes
      description: Es wird nach dem Rezept gesucht und alle Zutaten zurückgesandt
      parameters:
        - in: path
          name: recipeId
          required: true
          description: eine Id eines Rezepts
          schema:
            type: integer
            format: int256
      responses:
        "200":
          description: erfolgreich erhalten
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ingredient"
  /recipes/{recipeId}/ingredients/{name}:
    get:
      tags:
        - ingredient
      summary: Holt eine bestimmte Zutat eines Rezepts
      description: Es wird nach dem Rezept gesucht und die bestimmte Zutat zurückgesandt
      parameters:
        - in: path
          name: recipeId
          required: true
          description: eine Id eines Rezepts
          schema:
            type: integer
            format: int256
        - in: path
          name: name
          required: true
          description: Name der Zutat
          schema:
            type: string
      responses:
        "200":
          description: erfolgreich erhalten
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ingredient"
        "404":
          description: id nicht gefunden
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
  /accounts/recipes/{recipeId}/ingredients:
    post:
      tags:
        - ingredient
      summary: Fügt einem Rezept eine Zutat hinzu
      description: Es wird mithilfe der mitgegebenen id als Parameter, dem entsprechenden Rezept einen Zutat hinzugefügt.
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
        - in: path
          name: recipeId
          required: true
          description: eine Id eines Rezepts
          schema:
            type: integer
            format: int256
      responses:
        "200":
          description: erfolgreich hinzugefügt
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ingredient"
        "403":
          description: nicht berechtigt
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
    delete:
      tags:
        - ingredient
      summary: Removes all ingredients from an recipe
      description: Remove all ingredients from an recipe with the given recipe id
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
        - in: path
          name: recipeId
          required: true
          description: ID of an recipe
          schema:
            type: integer
            format: int256
      responses:
        "200":
          description: successfully removed
        "403":
          description: Unauthorized
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: Internal server error
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
  /accounts/recipes/{recipeId}/ingredients/{name}:
    delete:
      tags:
        - ingredient
      summary: Entfernt eine Zutat von einem Rezept
      description: Es wird mithilfe der mitgegebenen id als Parameter, dem entsprechenden Rezept einen Zutat entzogen.
      security:
        - bearerAuth: []
      parameters:
        - in: header
          name: Bearer
          required: true
          description: Encrypted JWT token of the session with general informations and accound id
          schema:
            $ref: "#/components/schemas/bearer"
        - in: path
          name: recipeId
          required: true
          description: eine Id eines Rezepts
          schema:
            type: integer
            format: int256
        - in: path
          name: name
          required: true
          description: Name of an ingredient
          schema:
            type: string
      responses:
        "200":
          description: erfolgreich entfernt
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: "#/components/schemas/ingredient"
        "403":
          description: nicht berechtigt
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
        "500":
          description: interner Fehler
          content:
            application/json:
              schema:
                $ref: "#/components/schemas/error"
components:
  securitySchemes:
    bearerAuth: # arbitrary name for the security scheme
      type: http
      scheme: bearer
      bearerFormat: JWT # optional, arbitrary value for documentation purposes

  schemas:
    account:
      type: object
      properties:
        id:
          type: string
          example: "99a036a7-c0f8-4bea-9bdb-2e6f19ae6d38"
        email:
          type: string
          example: "max.mustermann@web.de"
        name:
          type: string
          example: "franz"
        password:
          type: string
          format: hashed
          example: oKhfkLj8xkv32
    accountWithoutPassword:
      type: object
      properties:
        id:
          type: string
          example: "99a036a7-c0f8-4bea-9bdb-2e6f19ae6d38"
        email:
          type: string
          example: "max.mustermann@web.de"
        name:
          type: string
          example: "franz"
    profile:
      type: object
      properties:
        id:
          type: string
          example: "99a036a7-c0f8-4bea-9bdb-2e6f19ae6d38"
        name:
          type: string
          example: "franz"
    recipe:
      type: object
      properties:
        id:
          type: integer
          format: int256
          example: 1
        name:
          type: string
          example: "Pfannkuchen"
        author:
          type: integer
          example: 1
        isPublic:
          type: boolean
          example: true
        picture:
          type: string
          format: URL
          example: "https://www.example.com/1.jpg"
        instructions:
          type: string
          format: text
          example: "Do this, then this."
        creationDate:
          type: string
          format: date
          example: "2022-01-01"
        totalTime:
          type: integer
          format: int256
          example: 100
        prepTime:
          type: integer
          format: int256
          example: 50
        difficulty:
          type: object
          properties:
            index:
              type: integer
              format: int256
              example: 1
            value:
              type: string
              example: "medium"
    recipeCreate:
      type: object
      properties:
        name:
          type: string
          example: "Pfannkuchen"
        isPublic:
          type: boolean
          example: true
        picture:
          type: string
          format: URL
          example: "https://www.example.com/1.jpg"
        instructions:
          type: string
          format: text
          example: "Do this, then this."
        totalTime:
          type: integer
          format: int256
          example: 100
        prepTime:
          type: integer
          format: int256
          example: 50
        difficulty:
          type: object
          properties:
            index:
              type: integer
              format: int256
              example: 1
            value:
              type: string
              example: "medium"
    favorite:
      type: object
      properties:
        recipeId:
          type: integer
          format: int256
          example: 1;
        accountId:
          type: integer
          format: int256
          example: 1
    ingredient:
      type: object
      properties:
        recipeId:
          type: integer
          format: int256
          example: 1;
        name:
          type: string
          example: Banane
        amount:
          type: number
          format: float
          example: 2.5
        unit:
          type: object
          properties:
            index:
              type: integer
              format: int256
              example: 1
            value:
              type: string
              example: "ml"
    token:
      type: object
      properties:
        token:
          type: string
          description: JWT token für den Benutzer
          example: "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IlRlc3QiLCJpYXQiOjE1MTYyMzkwMjJ9.t4_WY-EOzCR-gin8IB9JLoxg9nKn8u86OVOOlkbzJok"
        expires:
          type: integer
          description: Ablauf Zeitpunkt des Tokens al Unixtimestap
          example: 1648906839
    bearer:
      type: object
      properties:
        header:
          type: object
          description: Header of token with general informations
          properties:
            alg:
              type: string
              description: Encryption algorithm
              example: "HS256"
            type:
              type: string
              description: Type of token
              example: "JWT"
        payload:
          type: object
          description: Data stored in the token
          properties:
            accountId:
              type: string
              description: ID of a specific account
              example: "ffa65506-1eae-4397-88e8-dc079925f3ec"
            iat:
              type: number
              description: Time the token was created
              example: 1650396655
            exp:
              type: number
              description: Time the token will expire
              example: 1650414655
    error:
      type: object
      properties:
        message:
          type: string
          description: Grund des Fehlers
          example: "Authorisation failed"
        hints:
          type: array
          items:
            example: "Password not correct"
            type: string

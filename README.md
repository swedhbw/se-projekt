# Welcome to Openthefridge!

Everyone knows the feeling, stomach growling, the saliva flowing, but then you look in the fridge and you have no idea what you could possibly cook with the ingredients lying there. A well known problem, but that's the end of it now. On this website you can enter your ingredients and get selected recipes that contain the ingredients you put into the search. Additional filters ensure that nothing is displayed that you do not want to see.

### Don't let the fridge win, start cooking!

Your Openthefridge Team 

[Live Website](https://squizl.devcheck.tk)


For technical documentation please consult the README from frontend and backend.